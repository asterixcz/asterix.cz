Title: Úvod
slug: index
order: 01

Asterix a.s. je renomovaným výrobcem a dodavatelem zařízení pro betonárny a patří ke špičce výrobců [řídicích systémů](https://asterix.cz/pages/ridici-systemy.html) pro betonárny všech typů. Firma byla založena v roce 1991 a za svou více než 30-ti letou historii realizovala přes 230 řídicích systémů betonáren v České republice a na Slovensku.

Firma je dodavatelem významných výrobců betonu jako ZAPA beton, ZAPA SK, Cemex, Skanska, B&BC, ŽPSV, MANE beton, KAROVIČ SK a dalších. Snahou je dodávat řídicí systémy v nejvyšší kvalitě a zajišťovat rychlý a účinný servis.

[<img alt="zapa" src="/images/zapa.png" style="height: 40px;"/>](http://www.zapa.cz/uvod/)
[<img alt="cemex" src="/images/cemex.png" style="height: 40px;"/>](http://cemex.cz/)
[<img alt="skanska" src="/images/skanska.png" style="height: 40px;"/>](http://www.skanska.cz/)
[<img alt="bbc" src="/images/bbc.png" style="height: 40px;"/>](http://www.babc.cz/)
[<img alt="zpsv" src="/images/zpsv.png" style="height: 40px;"/>](https://www.zpsv.cz/)
[<img alt="mane" src="/images/mane.png" style="height: 40px;"/>](http://www.mane.cz/cs/)
[<img alt="karovic" src="/images/karovic.png" style="height: 40px;"/>](http://www.karovic.sk/)

Firma si v průběhu let vybudovala takovou pozici na trhu, že začala definovat standardy v oblasti [řídicích systémů](https://asterix.cz/pages/ridici-systemy.html). Standardy se týkají přesnosti vážení a učících se algoritmů, maximalizace rychlosti betonárny, kvalita a rychlost poskytovaného servisu, vzdáleného připojení, respektování hustoty kalové vody a vlhkosti kameniva.

Kromě řídicích systému nabízí Asterix a.s. i další [návazné produkty](https://asterix.cz/pages/ostatni-produkty.html), např. řízení recyklačního zařízení, dopravy kameniva, distribuční rozváděč a je výhradním dovozcem vlhkoměrných sond a hustoměru kalové vody firmy WERNE & THIEL sensortechnic (dříve ARNOLD automation) pro Českou republiku a Slovensko.

Asterix a.s. je držitelem certifikátu jakosti ČSN EN ISO 9001 : 2016 s mezinárodní platností pro obor "Řídicí systémy pro průmyslovou automatizaci".

[![ESC](/images/thumbs/esc.png)](/images/esc.png)
[![ESC](/images/thumbs/esc.jpg)](/images/esc.jpg)
