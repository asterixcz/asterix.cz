Title: Ostatní produkty
order: 03

# Ostatní produkty #

## Technický a profylaktický servis ##

Technický a profylaktický servis řídicích systémů, dispečerských systémů a dalšího technologického zařízení betonárny zahrnuje:

1. Neomezený počet bezplatných výjezdů k provedení vyžádaných oprav servisovaného zařízení s dobou zásahu nejpozději do 24 hodin v pracovní dny.
1. Pravidelnou preventivní prohlídku servisovaného zařízení v časovém rozsahu 2x za rok.
1. Neomezenou poradenskou a konzultační službu prostřednictvím telefonu.
1. Pohotovostní službu ve dnech pracovního klidu na vyžádání předem.
1. U zvoleného typu servisu "A" veškerý spotřebovaný materiál a náhradní díly potřebné k opravám servisovaného zařízení platí zákazník. U zvoleného typu servisu "B" je veškerý spotřebovaný materiál a náhradní díly potřebné k opravám servisovaného zařízení zahrnut v paušální ceně. V ceně  není obsažen spotřební materiál (paměťová média, pásky tiskáren, tabelační papír apod.)
1. Cena technického a profylaktického servisu se řídí platným ceníkem.

## ATX D300 ##

Tento program provozovaný pod operačním systémem Windows splňuje požadavky normy EN 206+A2.

Dle požadavků uživatele lze program vybavit propojením na jiné ekonomické systémy a zajistit tak potřebnou integraci do větších celků zpracování dat.

Dispečerský program ATX D300 umožňuje práci nejméně dvou uživatelů v režimu čtení/zápis (dispečer + míchač) a dalších uživatelů v režimu čtení.

Všechny funkce jsou jištěny přístupovými právy uživatele a opatřeny kvalitní nápovědou. Obsluha dispečerského pracoviště přijímá objednávky na výrobu betonových směsí, zpracovává je a eventuálně odesílá na betonárnu. Odesílání se při propojení provádí automaticky, současně se tiskne dodací list a vytvářejí se podklady pro fakturaci. Obsluha má k dispozici databázi zákazníků, staveb, dodavatelů, řidičů a vozidel s řadou uložených údajů, což umožňuje rychlé přijetí objednávky, kontrolu dodržování platebních podmínek apod. Program zároveň zajišťuje evidenční a statistické funkce, související s provozem betonárny jako celku včetně skladového hospodářství.

## ATX COMFORT ##

**Elektronický ovládací systém nové generace pro ovládání výroby betonové směsi, splňující požadavky mezinárodních norem.**

Tento ovládací systém umožňuje komplexní plně automatické řízení celého procesu výroby betonové směsi  pomocí počítače IBM PC kompatibilní nebo manuální ovládání betonárny bez použití PC se zachováním základních technologických blokací.

ATX COMFORT obsahuje průmyslovou jednotku vstupů a výstupů pro komunikaci s PC, oddělené stejnosměrné zdroje, pomocná relé a svorkovnice pro spojení s prvky technologického zařízení. Ovládací panel zobrazuje stav technologie a umožňuje ji ručně ovládat. Integrované zobrazovací jednotky vah umožňují připojení všech druhů snímačů vážení od různých výrobců. Předností je i snadné tárování a jednoduchá kalibrace. Připojení řídicího PC je provedeno stíněným kabelem, vzdálené bezpečnostní tlačítko TOTAL STOP je připojeno dvoužilovým vodičem a silový rozvaděč se připojuje pomocí svorkovnice.

Rozvaděč o rozměrech 800x1000x300 mm je podle vybavení dodáván v  několika různých provedeních. Základní provedení je osazeno komponenty pro navažování kameniva, cementu, vody a přísad, přičemž a obsahuje čtyři displeje vah. Plné provedení navíc disponuje zobrazením navažování pro další nezávislé váhy přísad a vody. Na zvláštní přání je možno dodat elektronickou sběrnici v provedení INTERBUS.

Ovládací systém ATX COMFORT svou vysokou technickou úrovní, použitou kvalitní technikou a bezkonkurenční spolehlivostí vyhovuje i těm nejpřísnějším kritériím.

## Třífázový konzistoměr ##

*pro měření konzistence betonové směsi v průběhu mísení, standardní výstup 0-10V*

Konzistoměr betonové směsi fy. Asterix a.s. je přístroj, jehož výchylka je díky dokonalému elektronickému zpracování přímo úměrná činnému příkonu motoru (popřípadě motorů) míchačky v celém rozsahu možné zátěže.

Tento činný příkon motoru pak z převážné části závisí na tom, jak velký mechanický odpor klade materiál v míchačce lopatkám míchačky a tudíž výchylka měřicího přístroje, úměrná energii spotřebovávané na přetváření betonové směsi, udává konzistenci této směsi s velmi dobrou vypovídací schopností.

Nastavení celého přístroje (závisí na typu a velikosti míchačky) se provádí při instalaci přímo u uživatele.

V praxi to znamená, že pokud je namíchána záměs, jejíž konzistence je následně nezávisle změřena a prohlášena za správnou, je pak obsluha míchačky schopna upravit konzistenci dalších stejných záměsí pomocí konzistoměru a eventuálního dodávkování přídavné vody tak, aby konzistence odpovídala této záměsi původní.

## Distribuční rozvaděč, řízení dopravy kameniva, řízení recyklačního zařízení ##

V případě potřeby jsme schopni zajistit i dodávku elektrických zařízení, které s řídicím systémem betonárny přímo nesouvisí. Tato zařízení umisťujeme zpravidla do samostatných skříní, aby je šlo po konci životnosti snadno nahradit. Řízení dopravy kameniva a řízení recyklinku se totiž od řídicího systému betonárny odlišují jiným životním cyklem.

## Vlhkoměrné sondy a vyhodnocovací jednotky hustoty kalové vody WERNE & THIEL sensortechnic (dříve ARNOLD automation) ##

### FSA ###

Vlhkoměrná sonda s termoplastickým měřícím povrchem a držákem pro umístění do proudu materiálu v zásobníku.

[![FSA](/images/thumbs/fsa.jpg)](/images/fsa.jpg)

* Kryt a držák z antikorozní oceli, voděodolný vnitřek
* Možnost posunu na rameni o délce 0,5 a 1 m
* 3m krytý kabel sondy
* Měření teploty do 50 °C
* Upevnění šrouby M6 na čtyřech místech
* Napěťový výstup 0-10 V, napájení ± 15 Vss

Volitelně: proudový výstup 0-20 mA nebo 4-20 mA, povrch sondy keramický nebo z tvrzené pryže, měření teploty do 80°C.

### OLAS - přístroj na měření množství kalu v kalové vodě ###

Senzor na bázi absorpce světla Optical Light Absorption Sensor (OLAS) je měřicí přístroj k určování ztráty světla při průchodu médiem, v širším smyslu měřicí slouží přístroj k určování složení směsi. Pomocí senzoru OLAS lze určit např. obsah pevných látek (hustotu, zakalení) vodnaté emulze, ale i tloušťku fólií a povlaků.

[![OLAS](/images/thumbs/olas.jpg)](/images/olas.jpg)

OLAS prosvítí médium intenzivním měřicím světlem a zachytí s vysokou přesností množství měřicího světla, které médium propustí. Z tohoto množství OLAS vypočítá světelnou absorpci média.

OLAS přichází do kontaktu s médiem pomocí dvou optických kabelů. Jeden z těchto kabelů přivádí intenzivní měřicí světlo do média a druhý kabel odvádí světlo zeslabené při průchodu médiem zpět do přístroje. Citlivá elektronika se tak nedostane do přímého styku s médiem, což zaručuje vysokou životnost a spolehlivost zařízení.

Výhody pro betonárenský průmysl:

* Vyhovění normě EN 1008 o záměsové vodě do betonu, kdy obsluha již nemusí každý den ručně provádět kontrolu kalové vody (úspora času a byrokracie).
* Lepší využívání kalové vody, to znamená její menší spotřeba pokud je hustota příliš vysoká, a naopak vyšší spotřeba, pokud je hustota kalové vody nízká.
* Menší kolísání konzistence při výrobě betonu, tudíž i menší důvod k používání korekce vody vody při výrobě.
* Konkurenční výhoda při udělování zakázek oproti betonárnám, které přístroj OLAS nevyužívají. 
