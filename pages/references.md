Title: Reference
order: 04

# Reference #

Aktualizace: srpen 2023

## 2023 ##

* **252. Elektrizace železnic Praha a.s., pojízdná železniční betonárna PB-18 č.5**
    * Pojízdná železniční betonárna PB-18. Dodávka řídicího, ovládacího a silového elektrosystému.
    * Řídicí SW ATX - B300.
    * Do provozu uvedeno v srpnu 2023.
* **251. ZAPA beton a.s., - Ostrava - Heřmanice II**
    * Betonárna 2 m3. Dodávka řídicího, ovládacího a silového elektrosystému. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX - B400.
    * Do provozu uvedeno v červnu 2023.
* **250. CEMEX Czech Republic, s.r.o., - Nové Strašecí**
    * Betonárna 0,5 m3. Dodávka řídicího, ovládacího a silového elektrosystému. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX - B400.
    * Do provozu uvedeno v květnu 2023.
* **249. CEMEX Czech Republic, s.r.o., - Praha - Libuš II**
    * Betonárna 2,25 m3. Dodávka řídicího, ovládacího a silového elektrosystému. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX - B400.
    * Do provozu uvedeno v dubnu 2023.
* **248. ŽPSV a.s., - Čerčany II**
    * Betonárna 2 m3. Dodávka řídicího, ovládacího a silového elektrosystému. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX - B400.
    * Do provozu uvedeno v dubnu 2023.
* **247. ZAPA beton a.s., - Říčany II**
    * Betonárna 2 m3. Dodávka řídicího, ovládacího a silového elektrosystému. Dodávka třífázového konzistoměru. Dodávka frekvenčního měniče.
    * Řídicí SW ATX - B400.
    * Do provozu uvedeno v dubnu 2023.
* **246. ZAPA UNISTAV s.r.o., - Brno - Přízřenice II**
    * Betonárna Stetter 2 m3. Dodávka řídicího, ovládacího a silového elektrosystému. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX - B400.
    * Do provozu uvedeno v únoru 2023.
* **245. CEMEX Czech Republic, s.r.o., – Teplice II**
    * Betonárna 1,5 m3. Dodávka řídicího, ovládacího a silového elektrosystému. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX - B400.
    * Do provozu uvedeno v lednu 2023.

## 2022 ##

* **244. REYLON s.r.o., – Kostolné Kračany**
    * Dodávka řídicího, ovládacího a silového elektrosystému. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX - B400.
    * Dispečerský SW ATX - D400.
    * Do provozu uvedeno v prosinci 2022.
* **243. ZAPA beton a.s., Olomouc - Holice II**
    * Betonárna 2 m3. Dodávka řídicího, ovládacího a silového elektrosystému. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX - B400.
    * Do provozu uvedeno v říjnu 2022.
* **242. Pila Zdiměřice s.r.o., - Načeradec II**
    * Betonárna 0,8 m3. Dodávka řídicího, ovládacího a silového elektrosystému. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX - B400.
    * Do provozu uvedeno v červenci 2022.
* **241. BEZEDOS s.r.o., – Hronov II (Prefa)**
    * Betonárna ELBA 0,5 m3. Dodávka řídicího, ovládacího a silového elektrosystému. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX - B400.
    * Dispečerský SW ATX - D300.
    * Do provozu uvedeno v červnu 2022.
* **240. Elektrizace železnic Praha a.s., pojízdná železniční betonárna PB-18 č.7**
    * Pojízdná železniční betonárna PB-18. Dodávka řídicího, ovládacího a silového elektrosystému.
    * Řídicí SW ATX - B300.
    * Do provozu uvedeno v květnu 2022.
* **239. MANE BETON a.s., – České Budějovice II**
    * Betonárna 1 m3. Dodávka řídicího, ovládacího a silového elektrosystému. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX - B400. 
    * Do provozu uvedeno v únoru 2022.

## 2021 ##

* **238. NETO HOLDING s.r.o., – Ostrava**
    * Betonárna 3 m3. Dodávka řídicího, ovládacího a silového elektrosystému. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX - B400. 
    * Do provozu uvedeno v říjnu 2021.
* **237. RBR Betón, a.s., – Lovinobaňa**
    * Kontinuální betonárna SBM EMX400CC. Dodávka řídicího, ovládacího a silového elektrosystému. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX - B400. 
    * Do provozu uvedeno v září 2021.    
* **236. STAVEX CZ s.r.o., – Kácov**
    * Betonárna SB 1 m3. Dodávka řídicího, ovládacího a silového elektrosystému. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX - B400. 
    * Do provozu uvedeno v červnu 2021.
* **235. STEMRO s.r.o., – Rokytnice nad Jizerou**
    * Dodávka řídicího, ovládacího a silového elektrosystému. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX - B400.
    * Dispečerský SW ATX - D300.
    * Do provozu uvedeno v dubnu 2021.
* **234. EUROVIA CS, a.s. – Ostředek - SBM Euromix 4000**
    * Betonárna SBM Euromix 4000. Dodávka řídicího software a vstupně-výstupní jednotky. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX - B400.
    * Dispečerský SW ATX - D300.
    * Do provozu uvedeno v březnu 2021.

## 2020 ##

* **233. K-spol., s.r.o. – Pezinok**
    * Dodávka řídicího, ovládacího a silového elektrosystému. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX - B300.
    * Do provozu uvedeno v srpnu 2020.
* **232. ZAPA beton a.s. – Prostějov II**
    * Betonárna STASIS CE 55.11. Dodávka řídicího, ovládacího a silového elektrosystému. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX - B300.
    * Do provozu uvedeno v dubnu 2020.
* **231. ZAPA beton a.s. – Dolní Dunajovice**
    * Betonárna Stetter M2. Dodávka řídicího, ovládacího a silového elektrosystému. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX - B300.
    * Do provozu uvedeno v březnu 2020.
* **230. ZAPA beton SK s.r.o. – Košice**
    * Betonárna 2 m3. Dodávka řídicího, ovládacího a silového elektrosystému. Dodávka distribučního rozvaděče. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX - B300.
    * Do provozu uvedeno v únoru 2020.
* **229. ZAPA beton SK s.r.o. – Prešov**
    * Betonárna 2 m3. Dodávka řídicího, ovládacího a silového elektrosystému. Dodávka distribučního rozvaděče. Dodávka frekvenčního měniče. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX - B300.
    * Do provozu uvedeno v lednu 2020.

## 2019 ##

* **228. K-spol., s.r.o. – Trnava**
    * Betonárna 2 m3. Dodávka řídicího, ovládacího a silového elektrosystému. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX - B300.
    * Do provozu uvedeno v říjnu 2019.
* **227. ZAPA beton a.s. – Mladá Boleslav II**
    * Betonárna Stetter M2. Dodávka řídicího, ovládacího a silového elektrosystému. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX - B300.
    * Do provozu uvedeno v srpnu 2019.
* **226. IMC Holding spol. s r.o. – Velké Hamry**
    * Betonárna ELBA EMM 30. Dodávka řídicího, ovládacího a silového elektrosystému. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX - B400.
    * Do provozu uvedeno v srpnu 2019.
* **225. ZAPA beton SK s.r.o. – Bratislava Stavitelská**
    * Mobilní betonárna 2,5 m3 Simem Eagle 4000. Dodávka řídicího, ovládacího a silového elektrosystému. Dodávka frekvenčního měniče. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX - B300.
    * Do provozu uvedeno v červnu 2019.
* **224. ZAPA beton SK s.r.o. – Banská Bystrica**
    * Betonárna Merko 1 m3. Dodávka řídicího, ovládacího a silového elektrosystému. Dodávka distribučního rozvaděče. Dodávka frekvenčního měniče. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX - B300.
    * Do provozu uvedeno v březnu 2019.
* **223. ZAPA beton SK s.r.o. – Hlohovec**
    * Betonárna Stetter 1 m3. Dodávka řídicího, ovládacího a silového elektrosystému. Dodávka distribučního rozvaděče a dopravy kameniva. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX - B300.
    * Do provozu uvedeno v únoru 2019.
* **222. ZAPA beton SK s.r.o. – Sereď**
    * Betonárna Merko 1 m3. Dodávka řídicího, ovládacího a silového elektrosystému. Dodávka distribučního rozvaděče. Dodávka frekvenčního měniče. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX - B300.
    * Do provozu uvedeno v lednu 2019.
* **221. IMC Holding spol. s r.o. – Vlašim**
    * Betonárna 1 m3. Dodávka řídicího, ovládacího a silového elektrosystému. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX - B300.
    * Do provozu uvedeno v lednu 2019.

## 2018 ##

* **220. ZAPA beton SK s.r.o. – Trebišov**
    * Betonárna Merko 1 m3. Dodávka řídicího, ovládacího a silového elektrosystému. Dodávka distribučního rozvaděče. Dodávka frekvenčního měniče. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX - B300.
    * Do provozu uvedeno v listopadu 2018.
* **219. ZAPA beton a.s. – Hradec Králové II**
    * Mobilní betonárna 2,5 m3 Simem Eagle 4000. Dodávka řídicího, ovládacího a silového elektrosystému. Dodávka frekvenčního měniče. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX - B300.
    * Do provozu uvedeno v listopadu 2018.
* **218. IMC Holding spol. s r.o. – Mnichovo Hradiště**
    * Betonárna 1 m3. Dodávka řídicího, ovládacího a silového elektrosystému. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX - B300.
    * Do provozu uvedeno v srpnu 2018.
* **217. ZAPA beton a.s. – Plzeň**
    * Mobilní betonárna 2,5 m3 Simem Eagle 4000. Dodávka řídicího, ovládacího a silového elektrosystému. Dodávka frekvenčního měniče. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX - B300.
    * Do provozu uvedeno v červenci 2018.
* **216. ZAPA beton SK s.r.o. – Žilina**
    * Betonárna Merko HBS 100. Dodávka řídicího, ovládacího a silového elektrosystému. Dodávka distribučního rozvaděče. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX - B300.
    * Do provozu uvedeno v červnu 2018.
* **215. NETO HOLDING s.r.o. – Frýdek-Místek**
    * Betonárna 1 m3. Dodávka řídicího, ovládacího a silového elektrosystému. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX - B300.
    * Do provozu uvedeno v červnu 2018.
* **214. ZAPA beton SK s.r.o. – Bratislava-Vlčie Hrdlo**
    * Betonárna Rapid Simem. Dodávka řídicího, ovládacího a silového elektrosystému. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX - B300.
    * Do provozu uvedeno v květnu 2018.
    * V roce 2021 betonárna přestěhována do Strážnice.
* **213. Karovič s.r.o. – Malacky**
    * Mobilní betonárna. Dodávka řídicího, ovládacího a silového elektrosystému. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX - B300.
    * Do provozu uvedeno v dubnu 2018.
* **212. Elektrizace železnic Praha a.s., pojízdná železniční betonárna PB-18 č.1**
    * Pojízdná železniční betonárna PB-18. Kompletní dodávka řídicího, ovládacího a silového elektrosystému.
    * Řídicí SW ATX - B300.
    * Do provozu uvedeno v březnu 2018.
* **211. ZAPA beton SK s.r.o. – Most pri Bratislave II**
    * Betonárna STASIS. Dodávka řídicího, ovládacího a silového elektrosystému. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX - B300.
    * Do provozu uvedeno v březnu 2018.
* **210. Karovič s.r.o. – Bratislava-Vajnory**
    * Dodávka řídicího, ovládacího a silového elektrosystému. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX - B300.
    * Do provozu uvedeno v únoru 2018.
* **209. CEMEX Cement, s.r.o. – Praha-Stodůlky II**
    * Betonárna 2 m3. Dodávka řídicího, ovládacího a silového elektrosystému. Dodávka distribučního rozvaděče. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX - B300.
    * Do provozu uvedeno v únoru 2018.

## 2017 ##

* **208. ZAPA beton SK s.r.o. – Most pri Bratislave**
    * Mobilní betonárna 2,25 m3 Simem Eagle 4000. Dodávka řídicího, ovládacího a silového elektrosystému. Dodávka frekvenčního měniče. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX - B300.
    * Do provozu uvedeno v prosinci 2017.
* **207. CEMEX Czech Republic, s.r.o. – Správčice**
    * Betonárna 1 m3. Dodávka řídicího, ovládacího a silového elektrosystému. Dodávka distribučního rozvaděče a frekvenčního měniče. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX - B300.
    * Do provozu uvedeno v prosinci 2017.
* **206. ZAPA beton a.s. – Chvaletice**
    * Betonárna 1 m3. Dodávka řídicího, ovládacího a silového elektrosystému. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX - B300.
    * Do provozu uvedeno v listopadu 2017.
* **205. CEMEX Czech Republic, s.r.o. – Děčín II**
    * Betonárna 1 m3. Dodávka řídicího, ovládacího a silového elektrosystému. Dodávka distribučního rozvaděče a frekvenčního měniče. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX - B300.
    * Do provozu uvedeno v říjnu 2017.
* **204. CEMEX Czech Republic, s.r.o. – Klecany**
    * Betonárna Merko 1 m3. Dodávka řídicího, ovládacího a silového elektrosystému. Dodávka distribučního rozvaděče a frekvenčního měniče. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX - B300.
    * Do provozu uvedeno v říjnu 2017.
* **203. CEMEX Czech Republic, s.r.o. – Ústí nad Labem**
    * Betonárna 1 m3. Dodávka řídicího, ovládacího a silového elektrosystému. Dodávka ovládání ORF. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX - B300.
    * Do provozu uvedeno v září 2017.
* **202. ZAPA beton a.s. – Brandýs nad Labem II**
    * Betonárna  Merko 2,25 m3. Dodávka řídicího, ovládacího a silového elektrosystému. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX - B300.
    * Do provozu uvedeno v září 2017.
* **201. CEMEX Czech Republic, s.r.o. – Praha-Libuš**
    * Betonárna 2 m3. Dodávka řídicího, ovládacího a silového elektrosystému. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX - B300.
    * Do provozu uvedeno v září 2017.
* **200. ZAPA beton a.s. – Lanškroun II**
    * Betonárna Merko 2 m3. Dodávka řídicího, ovládacího a silového elektrosystému. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX - B300.
    * Do provozu uvedeno v srpnu 2017.
* **199. EUROVIA CS a.s. – Koberovice**
    * Mobilní kontinuální betonárna SBM EUROMIX 3000 CC+. Dodávka řídicího software a vstupně-výstupní jednotky.
    * Řídicí SW ATX - B300.
    * Dispečerský SW ATX - D300.
    * Do provozu uvedeno v červenci 2017.
    * V průběhu let přestěhována betonárna do Plzně a Ostředku.
* **198. ZAPA beton a.s. – Vlašim**
    * Betonárna firmy Probet. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka distribučního rozvaděče. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX - B300.
    * Do provozu uvedeno v červenci 2017.
* **197. Metrostav a.s. – Psáře**
    * Mobilní betonárna SBM EUROMIX 2000 CC+. Kompletní dodávka řídicího, ovládacího a silového elektrosystému.
    * Řídicí SW ATX - B300.
    * Dispečerský SW ATX - D300 dlouhodobě zapůjčen.
    * Do provozu uvedeno v červenci 2017.
    * V průběhu let přestěhována betonárna do Příboru.
* **196. ZAPA beton SK s.r.o. – Skalica**
    * Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX - B300.
    * Do provozu uvedeno v červnu 2017.
* **195. ZAPA beton SK s.r.o. – Michalovce**
    * Betonárna Simem Eagle 4000. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX - B300.
    * Do provozu uvedeno v květnu 2017.
* **194. ZAPA beton SK s.r.o. – Višňové u Žiliny**
    * Betonárna Merko. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka distribučního rozvaděče a dopravy kameniva. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX - B300.
    * Do provozu uvedeno v dubnu 2017.
* **193. FRISCHBETON s.r.o., Cerekvice nad Bystřicí**
    * Přestěhovaní mobilní betonárna z Bohumína.
    * Řídicí SW ATX B300.
    * Do provozu uvedeno v dubnu 2017.
* **192. Karovič s.r.o. – Stupava**
    * Dodávka řídicího a ovládacího a silového elektrosystému. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX - B300.
    * Do provozu uvedeno v březnu 2017.
* **191. Karovič s.r.o. – Bratislava – Záhorská Bystrica**
    * Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX - B300.
    * Do provozu uvedeno v únoru 2017.

## 2016 ##

* **190. ZAPA beton a.s. – Praha-Písnice**
    * Betonárna Liebherr R 2.0. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka rozvaděče dopravy kameniva. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX - B300.
    * Do provozu uvedeno v prosinci 2016.
* **189. ZAPA beton a.s. – Slušovice**
    * Betonárna Stetter M2. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX - B300.
    * Do provozu uvedeno v září 2016.
* **188. ZAPA beton SK s.r.o. – Dubná Skala u Žiliny**
    * Betonárna Stetter M2. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX - B300.
    * Do provozu uvedeno v červenci 2016.
* **187. DESTRO s.r.o. – Kladno**
    * Mísicí jádro umožňující výrobu pro halu (PREFA) a transportbeton. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX - B300.
    * Dispečerský SW ATX - D300.
    * Do provozu uvedeno v červnu 2016.
* **186. ZAPA beton a.s., Praha - Kačerov III**
    * Betonárna MERKO. Kompletní rekonstrukce řídicího, ovládacího a silového elektrosystému. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX B300
    * Do provozu uvedeno v červnu 2016.
* **185. Elektrizace železnic Praha a.s., pojízdná železniční betonárna PB-18 č.2**
    * Pojízdná železniční betonárna PB-18. Kompletní dodávka řídicího, ovládacího a silového elektrosystému.
    * Řídicí SW ATX - B300.
    * Do provozu uvedeno v dubnu 2016.
* **184. Karovič s.r.o. – Bratislava - Petržalka**
    * Betonárna SBM. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX - B300.
    * Do provozu uvedeno v květnu 2016.
* **183. ZAPA beton a.s. – Hradec Králové**
    * Betonárna Stetter H 1,25. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka rozvaděče dopravy kameniva. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX - B300.
    * Do provozu uvedeno v březnu 2016.
    * Betonárna byla v roce 2018 nahrazena za jinou. Řídicí systém po repasi přestěhován na jinou lokalitu.
* **182. Metrostav a.s. – Ostředek**
    * Mobilní kontinuální betonárna SBM EUROMIX 3000 CC+. Dodávka řídicího software a vstupně-výstupní jednotky.
    * Řídicí SW ATX - B300.
    * Dispečerský SW ATX - D300.
    * Do provozu uvedeno v březnu 2016.
    * V průběhu let betonárna přemístěna k Českým Budějovicím, do Příboru a Velkého Meziříčí.
* **181. EKO ZAPA beton a.s., Zábřeh**
    * Rekonstrukce řízení atypické betonárny. Dodávka řídicího a ovládacího elektrosystému. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX B300.
    * Do provozu uvedeno v únoru 2016.
* **180. PREFA PRODUKT s.r.o. – Předměřice nad Labem 2**
    * Dvoumíchačkové mísicí jádro v panelárně. Kompletní dodávka řídicího, ovládacího a silového elektrosystému.
    * Řídicí SW ATX - B300.
    * Do provozu uvedeno v lednu 2016.
* **179. PREFA PRODUKT s.r.o. – Předměřice nad Labem 1**
    * Dvoumíchačkové mísicí jádro v panelárně. Kompletní dodávka řídicího, ovládacího a silového elektrosystému.
    * Řídicí SW ATX - B300.
    * Do provozu uvedeno v lednu 2016.

## 2015 ##

* **178. NETO HOLDING s.r.o., Frýdek-Místek**
    * Přestěhovaná mobilní betonárna STETTER M2 z Ostravy Mariánských Hor.
    * Řídicí SW ATX B300.
    * Do provozu uvedeno v září 2015.
* ** 177. Purum s.r.o. – Stráž pod Ralskem**
    * Směšovací jednotka pro likvidaci odpadních surovin. Kompletní dodávka řídicího, ovládacího a silového elektrosystému.
    * Řídicí SW ATX - B300.
    * Do provozu uvedeno v září 2015.
* **176. ZAPA beton SK s.r.o. - Humenné**
    * Mobilní betonárna SBM. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX - B300.
    * Do provozu uvedeno v červenci 2015.
* **175. ZAPA beton a.s., Vyškov**
    * Rekonstrukce řídicího systému atypické betonárny německé provenience. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX 300
    * Do provozu uvedeno v červnu 2015.
* **174. ZAPA beton a.s., Brandýs nad Labem**
    * Rekonstrukce řízení atypické betonárny. Dodávka řídicího a ovládacího elektrosystému. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX B300
    * Do provozu uvedeno v květnu 2015.
    * V roce 2017 byla betonárna nahrazena větší míchačkou s novým řízením. Betonárna je vedena pod novým záznamem.
* **173. DOPRASTAV a.s., Senec II.**
    * Původní betonárna STETTER přestěhovaná z Komárna.
    * Řídicí SW ATX B300.
    * Dispečerský SW ATX D300.
    * Do provozu uvedeno v dubnu 2015.
* **172. B&BC,a.s., Přeštice**
    * Betonárna Merko 2m3. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka třífázového konzistoměru. Dodávka vlhkoměrného zařízení WERNE & THIEL sensortechnic.
    * Řídicí SW ATX B300.
    * Dispečerský SW ATX D300.
    * Do provozu uvedeno v dubnu 2015.
* **171. Beton Union Plzeň s.r.o., Stříbro**
    * Betonárna Stasis C50. Kompletní dodávka řídicího, ovládacího a silového elektrosystému v provedení ATX COMPACT. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX B300.
    * Do provozu uvedeno v dubnu 2015.
* **170. DOBET, spol. s r.o., Uherský Brod II**
    * Betonárna Stetter 1,5 m3. . Kompletní dodávka řídicího, ovládacího a silového elektrosystému v provedení ATX COMPACT. Dodávka třífázového konzistoměru. Dodávka vlhkoměrného zařízení WERNE & THIEL sensortechnic.
    * Řídicí SW ATX B300.
    * Do provozu uvedeno v dubnu 2015.
* **169. DOBET, spol. s r.o., Ostrožská Nová Ves**
    * Betonárna 1 m3. Přemístění řídicího, ovládacího a silového elektrosystému v provedení ATX COMPACT z betonárny Uherský Brod spojené s rekonstrukcí. Zapojen původní konzistoměr.
    * Řídicí SW ATX B300.
    * Do provozu uvedeno v únoru 2015.

## 2014 ##

* **168. B&BC,a.s., Plzeň**
    * Betonárna 2,25m3. Kompletní náhrada řídicího, ovládacího a silového elektrosystému. Dodávka třífázového konzistoměru. Dodávka vlhkoměrného zařízení WERNE & THIEL sensortechnic.
    * Řídicí SW ATX B300.
    * Dispečerský SW ATX D300.
    * Do provozu uvedeno v říjnu 2014.
* **167. Jiří Školník STYLSTAV s.r.o., Městec Králové**
    * Přemístění betonárny STASIS SB 20 z BARX Constructions spol. s r.o., Vimperk. Kompletní zapojení řídicího, ovládacího a silového elektrosystému v provedení ATX COMPACT. Dodávka konzistoměru (umístěn v rozvaděči).
    * Řídicí SW ATX B300.
    * Dispečerský SW ATX D300.
    * Do provozu uvedeno v červnu 2014.
* **166. M-Bet s.r.o., Pardubice**
    * Přemístění mobilní betonárny STASIS SB 20 z BEZEDOS s.r.o., Náchod. Kompletní zapojení řídicího, ovládacího a silového elektrosystému v provedení ATX COMPACT. Dodávka konzistoměru (umístěn v rozvaděči).
    * Řídicí SW ATX B300.
    * Dispečerský SW ATX D300.
    * Do provozu uvedeno v dubnu 2014.
* **165. SKANSKA TRANSBETON s.r.o., Praha - Ruzyně II**
    * Atypická nová betonárna se zásobníkem kameniva STETTER, skipem s měkkým rozběhem (měnič) a míchačkou STASIS 1 m3. Rekonstrukce ovládacího a částečně silového rozvaděče.
    * Řídicí SW ATX 300
    * Do provozu uvedeno v březnu 2014.
* **164. ŽPSV a.s., Uherský Ostroh**
    * Betonárna 0,5m3. Náhrada konkurenčního řídicího systému. Dodávka řídicího a ovládacího elektrosystému. Oprava silového elektrosystému. Zapojen původní konzistoměr.
    * Řídicí SW ATX B300.
    * Dispečerský SW ATX D300.
    * Do provozu uvedeno v lednu 2014.
* **163. M - SILNICE a.s., Nový Bydžov**
    * Betonárna STASIS SB 20. Náhrada konkurenčního řídicího systému. Dodávka řídicího a ovládacího elektrosystému. Oprava silového elektrosystému.
    * Řídicí SW ATX B300.
    * Do provozu uvedeno v lednu 2014.

## 2013 ##

* **162. ZAPA beton SK s.r.o., Senica**
    * Přemístění betonárny ELBA z Neratovic. Repase původního řídicího, ovládacího a silového elektrosystému. Dodávka konzistoměru (umístěn v rozvaděči).
    * Řídicí SW ATX B300.
    * Do provozu uvedeno v srpnu 2013.
* **161. ZAPA beton a.s., Zlín - Malenovice**
    * Betonárna STETTER M05. Náhrada konkurenčního řídicího systému. Dodávka řídicího a ovládacího elektrosystému. Oprava silového elektrosystému. Zapojen původní konzistoměr.
    * Řídicí SW ATX B300.
    * Do provozu uvedeno v červenci 2013.
* **160. AZS 98, s.r.o., Plzeň - Valcha**
    * Betonárna 1m3. Náhrada konkurenčního řídicího systému. Kompletní dodávka řídicího a ovládacího elektrosystému. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX B300.
    * Dispečerský SW ATX D300.
    * Do provozu uvedeno v červnu 2013.
* **159. ZAPA beton a.s., Strážnice**
    * Přemístění betonárny HBS 50 ze Starého Města u Uherského Hradiště. Kompletní repase řídicího, ovládacího a silového elektrosystému. Dodávka konzistoměru (umístěn v rozvaděči).
    * Řídicí SW ATX B300.
    * Do provozu uvedeno v březnu 2013.
* **158. Beton Union Plzeň s.r.o., Příbram**
    * Betonárna MERKO MB2D. Kompletní dodávka řídicího, ovládacího a silového elektrosystému v provedení ATX COMPACT. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX B300.
    * Dispečerský SW ATX D300.
    * Do provozu uvedeno v lednu 2013.
    * Betonárna provozována novým majitelem v partnerství s Českomoravský beton, a. s.

## 2012 ##

* **157. BEZEDOS s.r.o., Vysokov**
    * Betonárna s míchačkou Pemat 1,25m3. Kompletní dodávka řídicího, ovládacího a silového elektrosystému v provedení ATX COMPACT. Dodávka třífázového konzistoměru.
    * Řídicí SW ATX B300.
    * Dispečerský SW ATX D300.
    * Do provozu uvedeno v září 2012.
* **156. ZAPA beton a.s., Neratovice**
    * Betonárna ELBA. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka třífázového konzistoměru. Připojení vlhkoměrného zařízení do řídicího systému.
    * Řídicí SW ATX B300.
    * Do provozu uvedeno v červnu 2012.
* **155. ZAPA beton a.s., Liberec**
    * Rekonstrukce řízení věžové betonárny STETTER. Dodávka řídicího a ovládacího elektrosystému. Dodávka třífázového konzistoměru konzistoměru.
    * Řídicí SW ATX 300
    * Do provozu uvedeno v březnu 2012.
* **154. ZAPA beton a.s., Praha - Kačerov IV**
    * Rekonstrukce řízení betonárny STETTER s míchačkou BAS 2 m3. Kompletní dodávka ovládacího a silového systému. Dodávka třífázového konzistoměru konzistoměru.
    * Řídicí SW ATX 300
    * Do provozu uvedeno v únoru 2012
* **153. B&BC,a.s., Zbůch**
    * Betonárna MERKO 2m3. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka konzistoměru. Připojení vlhkoměrného zařízení Hydronix do řídicího systému.
    * Řídicí SW ATX B300.
    * Dispečerský SW ATX D300.
    * Do provozu uvedeno v únoru 2012.

## 2011 ##

* **152. MANE BETON a.s., Týn nad Vltavou**
    * Mobilní betonárna 1,5 m3. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka konzistoměru (umístěn v rozvaděči).
    * Řídicí SW ATX B300.
    * Dispečrský SW ATX D300.
    * Do provozu uvedeno v říjnu 2011.
* **151. Karel Säckl, Albrechtice**
    * Přemístění betonárny STASIS SB 20 z ANDRLA, Branka u Opavy. Kompletní zapojení řídicího, ovládacího a silového elektrosystému v provedení ATX COMPACT.
    * Řídicí SW ATX B300.
    * Do provozu uvedeno v srpnu 2011.
* **150. AGROPOST s.r.o., Načeradec**
    * Přemístění betonárny STASIS SB 20 z BETON TVAR s.r.o., Žalmanov. Kompletní zapojení řídicího, ovládacího a silového elektrosystému v provedení ATX COMPACT.
    * Řídicí SW ATX B300.
    * Do provozu uvedeno v červenci 2011.
    * Právním nástupcem je RECTA s.r.o.
* **149. CEMEX Czech Republic, s.r.o., Týn nad Vltavou**
    * Betonárna STETTER 1m3. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka konzistoměru ve stolním provedení.
    * Řídicí SW ATX B300.
    * Do provozu uvedeno v červnu 2011.
* **148. ZAPA beton a.s., Holubice**
    * kompletní inovace řídicího systému v souvislosti s rozšířením technologického zařízení betonárny.
    * Řídicí SW ATX 300
    * Do provozu uvedeno v březnu 2011.
* **147. ZAPA beton a.s., Středokluky**
    * Rekonstrukce řídicího a ovládacího elektrosystému betonárny STASIS Ce-50. Dodávka konzistoměru (umístěn v rozvaděči).
    * Řídicí SW ATX 300
    * Do provozu uvedeno v únoru 2003.
* **146. ZAPA beton a.s., Ostrava - Heřmanice**
    * Betonárna Stetter H1. Náhrada konkurenčního řídicího systému. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka konzistoměru (umístěn v rozvaděči).
    * Řídicí SW ATX B300.
    * Do provozu uvedeno v lednu 2011.

## 2010 ##

* **145. B&BC,a.s., Zruč u Plzně**
    * Betonárna MERKO 1m3. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka konzistoměru a vlhkoměrného zařízení WERNE & THIEL sensortechnic.
    * Řídicí SW ATX B300.
    * Dispečerský SW ATX D300.
    * Do provozu uvedeno v září 2010.
* **144. ZAPA beton a.s., Chotěboř**
    * Přemístění betonárny ELBA z Přerova. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka konzistoměru (umístěn v rozvaděči).
    * Řídicí SW ATX B300.
    * Do provozu uvedeno v červenci 2010.
* **143. ZAPA beton a.s., Staré Město II.**
    * Betonárna MERKO HBS2D. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka konzistoměru (umístěn v rozvaděči).
    * Řídicí SW ATX B300.
    * Do provozu uvedeno v červnu 2010.
* **142. DSH-Dopravní stavby, a.s., Břest u Kroměříže**
    * Kontinuální betonárna BHS-250. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka konzistoměru a vlhkoměrného zařízení WERNE & THIEL sensortechnic.
    * Řídicí SW ATX B300.
    * Do provozu uvedeno v červnu 2010.
    * Betonárna je v současné době mimo provoz.
* **141. ZAPA beton a.s., Pohořelice**
    * Betonárna Transunit 0,75 m3. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka konzistoměru (umístěn v rozvaděči).
    * Řídicí SW ATX B300.
    * Do provozu uvedeno v dubnu 2010.
* **140. CEMEX Czech Republic, k.s. , Hradec Králové - Plačice**
    * Betonárna typu RANGER francouzské provenience. Rekonstrukce betonárny – výměna míchačky 2 m3 a výměna vážicího pasu. Nová kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka konzistoměru.
    * Řídicí SW ATX B300.
    * Do provozu uvedeno v dubnu 2010.
* **139. ZAPA beton a.s., Votice**
    * Přemístění betonárny STETTER M2 z Bravantic. Repase řídicího, ovládacího a silového elektrosystému. Dodávka konzistoměru (umístěn v rozvaděči).
    * Řídicí SW ATX B300.
    * Do provozu uvedeno v dubnu 2010.

## 2009 ##

* **138. LOGEA s.r.o., Tuřany - Byseň (Slaný)**
    * Betonárna 1 m3. Oprava řídicího a ovládacího elektrosystému v provedení ATX COMPACT. Dodávka konzistoměru (umístěn v rozvaděči).
    * Řídicí SW ATX B300.
    * Dispečerský SW ATX D300.
    * Do provozu uvedeno v prosinci 2009.
    * Novým majitelem je CEMEX s.r.o.
* **137. Prefa HUBENOV s.r.o., Kaplice**
    * Betonárna SKAKO AM 750. Kompletní dodávka řídicího, ovládacího a silového elektrosystému v provedení ATX COMPACT. Dodávka konzistoměru (umístěn v rozvaděči).
    * Řídicí SW ATX B300.
    * Do provozu uvedeno v prosinci 2009.
* **136. BARX Constructions spol s r.o., Vimperk**
    * Betonárna STASIS SB-20. Kompletní dodávka řídicího, ovládacího a silového elektrosystému v provedení ATX COMPACT. Dodávka konzistoměru (umístěn v rozvaděči).
    * Řídicí SW ATX B300.
    * Dispečerský SW ATX D300.
    * Do provozu uvedeno v červenci 2009.
    * V roce 2014 betonárna přestěhovaná do Městce Králové s novým provozovatelem Školník Stylstav. Betonárna je vedena pod novým záznamem.
* **135. PB Transportbeton s.r.o., Otovice u Karlových Varů**
    * Betonárna STASIS CT-04. Kompletní dodávka řídicího, ovládacího a silového elektrosystému v provedení ATX COMPACT. Dodávka konzistoměru.
    * Řídicí SW ATX B300.
    * Dispečerský SW ATX D300.
    * Do provozu uvedeno v červnu 2009.
    * Od roku 2012 je betonárna provozována pod th-beton s.r.o.
    * Posledním známým provozovatelem je FRISCHBETON s.r.o.
* **134. ZAPA beton a.s., Aš**
    * Betonárna STETTER 2m3. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka konzistoměru (umístěn v rozvaděči).
    * Řídicí SW ATX B300.
    * Do provozu uvedeno v květnu 2009.
* **133. Betonárka Mukařov s.r.o., Mukařov**
    * Mobilní betonárna STASIS SB-20. Kompletní dodávka řídicího, ovládacího a silového elektrosystému v provedení ATX COMPACT. Dodávka konzistoměru (umístěn v rozvaděči).
    * Řídicí SW ATX B300.
    * Do provozu uvedeno v dubnu 2009.
* **132. ZAPA beton a.s., Kladno**
    * Betonárna STETTER 2m3. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka konzistoměru (umístěn v rozvaděči).
    * Řídicí SW ATX B300.
    * Do provozu uvedeno v dubnu 2009.
* **131. ŽPSV a.s., Nové Hrady**
    * Rekonstrukce dvoumíchačkového mísicího centra pro výrobu prefabrikátů. Kompletní dodávka dvojitého řídicího, ovládacího a silového systému včetně dodávky dvou konzistoměrů a vlhkoměrných systémů firmy WERNE & THIEL sensortechnic a dodávky systémů měření a regulace vlhkosti v míchačce Micomp Easy firmy Michenfelder.
    * Řídicí SW ATX B300.
    * Dispečerský SW ATX D300.
    * Do provozu uvedeno v dubnu 2009.
* **130. Štěrkovny spol s r.o. Dolní Benešov, Bohumín**
    * Mobilní betonárna. Kompletní dodávka řídicího, ovládacího a silového elektrosystému v provedení ATX COMPACT. Dodávka konzistoměru.
    * Řídicí SW ATX B300.
    * Do provozu uvedeno v březnu 2009.
    * Nový majetel je FRISCHBETON s.r.o.
    * Betonárna byla v roce 2017 přestěhována do Cerekvic a je vedena pod novým záznamem.
* **129. ZAPA beton a.s., Přerov**
    * Betonárna MERKO 2m3. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka konzistoměru (umístěn v rozvaděči).
    * Řídicí SW ATX B300.
    * Do provozu uvedeno v únoru 2009.
* **128. Štěrkovny spol s r.o. Dolní Benešov, Ostrava - Mariánské Hory**
    * Věžová betonárna STETTER V 1,5. Kompletní dodávka řídicího, ovládacího a silového elektrosystému v provedení ATX COMPACT. Dodávka konzistoměru.
    * Řídicí SW ATX B300.
    * Do provozu uvedeno v únoru 2009.
    * Novým majitelem je FRISCHBETON s.r.o.
    * CEMEX s.r.o. má betonárnu v podnájmu.
* **127. Štěrkovny spol s r.o. Dolní Benešov, Ostrava - Mariánské Hory**
    * Mobilní betonárna STETTER M2. Kompletní dodávka řídicího, ovládacího a silového elektrosystému v provedení ATX COMPACT. Dodávka konzistoměru.
    * Řídicí SW ATX B300.
    * Do provozu uvedeno v lednu 2009.
    * Novým majitelem je FRISCHBETON s.r.o.
    * V roce 2015 betonárna přestěhována do Frýdku-Místku. Novým majitelem je NETO HOLDING s.r.o. Betonárna je vedena pod novým záznamem.
* **126. ZAPA beton a.s., Hranice**
    * Náhrada řídicího systému DOS. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka konzistoměru (umístěn v rozvaděči).
    * Řídicí SW ATX B300.
    * Do provozu uvedeno v lednu 2009.

## 2008 ##

* **125. ZAPA beton a.s., Litvínov**
    * Betonárna MERKO HBS 2D. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka konzistoměru (umístěn v rozvaděči).
    * Řídicí SW ATX B300.
    * Do provozu uvedeno v prosinci 2008.
* **124. CEMEX Czech Republic, k.s., Pardubice**
    * Betonárna MERKO HBS 2D. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka konzistoměru.
    * Řídicí SW ATX B300.
    * Dispečerský SW ATX D300.
    * Do provozu uvedeno v listopadu 2008.
    * Betonárna byla počátkem roku 2018 odstavena.
* **123. BEZEDOS s.r.o., Náchod**
    * Mobilní betonárna STASIS SB-20. Kompletní dodávka řídicího, ovládacího a silového elektrosystému v provedení ATX COMPACT. Dodávka konzistoměru (umístěn v rozvaděči).
    * Řídicí SW ATX B300.
    * Dispečerský SW ATX D300.
    * Do provozu uvedeno v září 2008.
    * V dubnu 2014 betonárna přestěhována do Pardubic. Nový provozovatel M-Bet s.r.o. Betonárna je vedena pod novým záznamem.
* **122. FRISCHBETON s.r.o., Červená Voda**
    * Přestěhovaná mobilní betonárna STASIS SB 30 z Energy Invest Opatov.
    * Řídicí SW ATX B300.
    * Dispečerský SW ATX D300.
    * Do provozu uvedeno v září 2008.
* **121. DOBET spol. s r.o., Uherský Brod**
    * Betonárna 1m3. Kompletní dodávka řídicího, ovládacího a silového elektrosystému v provedení ATX COMPACT. Dodávka konzistoměru.
    * Řídicí SW ATX B300.
    * Do provozu uvedeno v srpnu 2008.
    * Betonárna byla v roce 2015 zrušena a nahrazena novou, která je vedena pod novým záznamem.
* **120. ZAPA beton a.s., Prostějov**
    * Náhrada řídicího systému DOS. Kompletní dodávka řídicího a ovládacího elektrosystému. Dodávka konzistoměru (umístěn v rozvaděči).
    * Řídicí SW ATX B300.
    * Do provozu uvedeno v srpnu 2008.
    * V roce 2020 byla betonárna nahrazena novým řízením. Betonárna je vedena pod novým záznamem.
* **119. ENERGY INVEST s.r.o., Třebovice (Opatov II.)**
    * Betonárna STASIS Ce-50. Kompletní dodávka řídicího, ovládacího a silového elektrosystému v provedení ATX COMPACT. Dodávka konzistoměru (umístěn v rozvaděči).
    * Řídicí SW ATX B300.
    * Dispečerský SW ATX D300.
    * Do provozu uvedeno v červnu 2008.
    * Současný provozovatel FRISCHBETON s.r.o.
* **118. SKANSKA TRANSBETON s.r.o., Praha - Uhříněves (Stetter)**
    * Rekonstrukce betonárny STETTER. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka konzistoměru a vlhkoměrného zařízení WERNE & THIEL sensortechnic.
    * Řídicí SW ATX B300.
    * V lednu 2008 provedena komplexní rekonstrukce řízení kamenivového hospodářství.
    * Do provozu uvedeno v dubnu 2008.
* **117. ZAPA beton a.s., Nové Město na Moravě**
    * Betonárna STETTER H1. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka konzistoměru (umístěn v rozvaděči).
    * Řídicí SW ATX B300.
    * Do provozu uvedeno v březnu 2008.
* **116. SKANSKA TRANSBETON s.r.o., Praha - Uhříněves (Merko)**
    * Betonárna MERKO . Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka konzistoměru a vlhkoměrného zařízení WERNE & THIEL sensortechnic.
    * Řídicí SW ATX B300.
    * Do provozu uvedeno v březnu 2008.
* **115. BEZEDOS s.r.o., Broumov**
    * Betonárna STASIS Ce-30. Kompletní dodávka řídicího, ovládacího a silového elektrosystému v provedení ATX COMPACT. Dodávka konzistoměru (umístěn v rozvaděči).
    * Řídicí SW ATX B300.
    * Dispečerský SW ATX D300.
    * Do provozu uvedeno v březnu 2008.

## 2007 ##

* **114. ŽPSV a.s., Běstovice**
    * Rekonstrukce mísicího centra pro výrobu prefabrikátů. Kompletní dodávka řídicího, ovládacího a silového elektrosystému v provedení ATX COMPACT. Dodávka konzistoměru.
    * Řídicí SW ATX B300.
    * Do provozu uvedeno v prosinci 2007.
    * V průběhu let byla betonárna zrušena a nahrazena novou s jiným řízením.
* **113. ZAPA beton a.s., Polička**
    * Přemístění betonárny STETTER M1 z Frýdku Místku. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka konzistoměru (umístěn v rozvaděči).
    * Řídicí SW ATX B300.
    * Do provozu uvedeno v září 2007.
* **112. BEZEDOS s.r.o., Hronov**
    * Mobilní betonárna Webaumix. Kompletní dodávka řídicího, ovládacího a silového elektrosystému v provedení ATX COMPACT. Dodávka konzistoměru (umístěn v rozvaděči).
    * Řídicí SW ATX B300.
    * Dispečerský SW ATX D300.
    * Do provozu uvedeno v srpnu 2007.
* **111. ENERGY INVEST s.r.o., Opatov I**
    * Mobilní betonárna STASIS SB 30. Kompletní dodávka řídicího, ovládacího a silového elektrosystému v provedení ATX COMPACT. Dodávka konzistoměru (umístěn v rozvaděči).
    * Řídicí SW ATX B300.
    * Dispečerský SW ATX D300.
    * Do provozu uvedeno v červenci 2007.
    * V září 2008 přemístěno do Červené Vody. Betonárna je vedena pod novým záznamem.
* **110. ŽPSV a.s., Čerčany**
    * Rekonstrukce dvoumíchačkového mísicího centra pro výrobu prefabrikátů. Kompletní dodávka dvojitého řídicího, ovládacího a silového systému včetně dodávky dvou konzistoměrů a vlhkoměrných systémů firmy ARNOLD - Automation a dodávky systému měření a regulace vlhkosti v míchačce Micomp Easy firmy Michenfelder.
    * Řídicí SW ATX B300.
    * Dispečerský SW ATX D300.
    * Do provozu uvedeno v červenci 2007.
* **109. Štěrkovny spol. s r.o. Dolní Benešov, Kopřivnice**
    * Betonárna STASIS Ce-55. Kompletní dodávka řídicího, ovládacího a silového elektrosystému v provedení ATX COMPACT. Dodávka konzistoměru a vlhkoměrného zařízení ARNOLD-Automation.
    * Řídicí SW ATX B300.
    * Dispečerský SW ATX D300.
    * Do provozu uvedeno v červnu 2007.
* **108. ZAPA beton a.s., Stráž pod Ralskem**
    * Betonárna MERKO. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka konzistoměru (umístěn v rozvaděči).
    * Řídicí SW ATX B300.
    * Do provozu uvedeno v březnu 2007.
* **107. ŽPSV a.s., Litice nad Orlicí**
    * Rekonstrukce dvou oddělených mísicích center pro výrobu prefabrikátů. Kompletní dodávka dvou řídicích, ovládacích a silových systémů včetně dodávky dvou konzistoměrů a vlhkoměrných systémů firmy ARNOLD - Automation.
    * Nahrazení původních řídicích systémů z roku 1997.
    * Řídicí SW ATX B300.
    * Dispečerský SW ATX D300.
    * Do provozu uvedeno v březnu 2007.

## 2006 ##

* **106. MANE Holding a.s., České Budějovice**
    * Oprava původního řídicího systému betonárny STASIS Ce-50. Dodávka řídicího a ovládacího elektrosystému.
    * Řídicí SW ATX B300.
    * Dispečerský SW ATX D300.
    * Do provozu uvedeno v prosinci 2006.
* **105. ZAPA beton a.s., Horoměřice**
    * Betonárna MERKO HBS 100. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka konzistoměru (umístěn v rozvaděči).
    * Řídicí SW ATX B300.
    * Do provozu uvedeno v listopadu 2006.
* **104. ZAPA beton a.s., Suchdol nad Odrou**
    * Betonárna MERKO HBS 2D. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka konzistoměru (umístěn v rozvaděči).
    * Řídicí SW ATX B300.
    * Do provozu uvedeno v září 2006.
* **103. SC XAVEROV Horní Počernice a.s., Praha - Horní Počernice**
    * Betonárna MERKO HBS 2D. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka konzistoměru (umístěn v rozvaděči) a vlhkoměrného zařízení ARNOLD-Automation.
    * Řídicí SW ATX B300.
    * Dispečerský SW ATX D300.
    * Do provozu uvedeno v červenci 2006.
    * Novým majitelem je firma CEMEX s.r.o.
* **102. ZAPA beton a.s., Jihlava**
    * Přemístění betonárny STETTER M2 z Rychnova na Moravě. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka konzistoměru (umístěn v rozvaděči) a vlhkoměrného zařízení ARNOLD-Automation.
    * Řídicí SW ATX B300.
    * Do provozu uvedeno v červenci 2006.
    * Stávajícím provozovatelem betonárny je Českomoravský beton a.s.
* **101. Štěrkovny spol. s r.o. Dolní Benešov, Odry**
    * Betonárna STASIS Ce-50. Kompletní dodávka řídicího, ovládacího a silového elektrosystému v provedení ATX COMPACT. Dodávka konzistoměru a vlhkoměrného zařízení ARNOLD-Automation.
    * Řídicí SW ATX B300.
    * Dispečerský SW ATX D300.
    * Do provozu uvedeno v červenci 2006.
* **100. ZAPA beton a.s., Hulín**
    * Betonárna MERKO HBS 100. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka konzistoměru (umístěn v rozvaděči).
    * Řídicí SW ATX B300.
    * Do provozu uvedeno v červenci 2006.
* **99. DOPRASTAV a.s., Komárno**
    * Rekonstrukce betonárny STETTER. Kompletní dodávka řídicího a ovládacího elektrosystému v provedení ATX COMFORT. Dodávka konzistoměru.
    * Řídicí SW ATX B300.
    * Dispečerský SW ATX D300.
    * Do provozu uvedeno v květnu 2006.
    * V roce 2015 betonárna přestěhována do Sence. Betonárna je vedená pod novým záznamem
* **98. SKANSKA TRANSBETON s.r.o., Praha - Chodov**
    * Rekonstrukce betonárny STETTER. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka konzistoměru a vlhkoměrného zařízení ARNOLD-Automation.
    * Řídicí SW ATX B300.
    * Do provozu uvedeno v dubnu 2006.
* **97. VAKSTAV spol. s r.o., Jablonné nad Orlicí**
    * Rekonstrukce betonárny STASIS SB 30 m3.Kompletní dodávka řídicího, ovládacího a silového elektrosystému v provedení ATX COMPACT.
    * Řídicí SW ATX B300.
    * Dispečerský SW ATX D300.
    * Do provozu uvedeno v březnu 2006.
* **96. Štěrkovny spol. s r.o. Dolní Benešov, Třinec**
    * Betonárna ELBA. Kompletní dodávka řídicího a ovládacího elektrosystému a generální oprava silové části betonárny. Dodávka konzistoměru (umístěn v rozvaděči).
    * Řídicí SW ATX B300.
    * Dispečerský SW ATX D300.
    * Do provozu uvedeno v březnu 2006.
* **95. BETON TYLL, Nebovidy**
    * Rekonstrukce betonárny STASIS SB 30 m3. Kompletní dodávka řídicího, ovládacího a silového elektrosystému v provedení ATX COMPACT. Připojení konzistoměru - dodávka z roku 2004.
    * Řídicí SW ATX B300.
    * Do provozu uvedeno v březnu 2006.
    * V průběhu let byla betonárna odprodána a zrušena. Poslední známá lokalita Červené Pečky majitel Agro Novák.

## 2005 ##

* **94. ZAPA beton a.s., Moravská Třebová**
    * Betonárna MERKO HBS 50. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka konzistoměru (umístěn v rozvaděči).
    * Řídicí SW ATX B300.
    * Do provozu uvedeno v listopadu 2005.
* **93. BETON TVAR s.r.o., Karlovy Vary, Žalmanov**
    * Rekonstrukce betonárny STASIS SB 20. Kompletní dodávka řídicího, ovládacího a silového elektrosystému v provedení ATX COMPACT. Dodávka dvojitého konzistoměru a dvojitého vlhkoměrného zařízení ARNOLD – Automation.
    * Řídicí SW ATX B300.
    * Do provozu uvedeno v říjnu 2005.
    * V roce 2011 betonárna přestěhována do Načeradce. Nový provozovatel AGROPOST s.r.o. Betonárna je vedena pod novým záznamem.
* **92. EKAS s.r.o., Česká Lípa**
    * Rekonstrukce betonárny ELBA 1,5 m3. Kompletní dodávka řídicího a ovládacího elektrosystému. Dodávka konzistoměru.
    * Řídicí SW ATX B300.
    * Do provozu uvedeno v červnu 2005.
* **91. ZAPA beton a.s., Vysoké Mýto**
    * Betonárna MERKO HBS 50. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka konzistoměru (umístěn v rozvaděči).
    * Řídicí SW ATX B300.
    * Do provozu uvedeno v červnu 2005.
* **90. SKANSKA TRANSBETON s.r.o., Praha – Řeporyje**
    * Rekonstrukce betonárny STETTER. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka konzistoměru a vlhkoměrného zařízení ARNOLD – Automation.
    * Řídicí SW ATX 300.
    * Do provozu uvedeno v dubnu 2005.
* **89. NEICO s.r.o., Slaný**
    * Rekonstrukce betonárny STASIS SB 20. Kompletní dodávka řídicího, ovládacího a silového elektrosystému v provedení ATX COMPACT.
    * Dodávka dvojitého konzistoměru a vlhkoměrného zařízení ARNOLD – Automation.
    * Řídicí SW ATX B300.
    * Do provozu uvedeno v dubnu 2005.
    * V březnu 2011 provedena rekonstrukce silové a ovládací části.
* **88. ZAPA beton a.s., Slaný**
    * Betonárna STETTER H1. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka konzistoměru (umístěn v rozvaděči).
    * Řídicí SW ATX B300.
    * Do provozu uvedeno v březnu 2005.
* **87. CEMEX Czech Republic, s.r.o., Liberec**
    * Betonárna STASIS Ce-50 s původním poloautomatem atypické konstrukce. Kompletní rekonstrukce ovládací části. Silovou část rekonstruovala jiná firma.
    * Řídicí SW původní ATX 250
    * Do provozu uvedeno v březnu 2005.
    * V roce 2012 přechod na SW ATX 300
* **86. ZAPA beton a.s., Ostrava – Hrabová**
    * Betonárna MERKO HBS 50. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka konzistoměru (umístěn v rozvaděči).
    * Řídicí SW ATX B300.
    * Do provozu uvedeno v únoru 2005.
* **85. ZAPA beton a.s., Tábor**
    * Betonárna STETTER M2. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka konzistoměru (umístěn v rozvaděči).
    * Řídicí SW ATX B300.
    * Do provozu uvedeno v lednu 2005.

## 2004 ##

* **84. EKO ZAPA beton a.s., Zábřeh**
    * Rekonstrukce atypické betonárny. Dodávka řídicího a ovládacího elektrosystému. Dodávka konzistoměru (umístěn v rozvaděči).
    * Řídicí SW ATX B300.
    * Do provozu uvedeno v prosinci 2004.
    * V únoru 2016 provedena kompletní rekonstrukce řízení. Betonárna je vedena pod novým záznamem.
* **83. ZAPA beton a.s., Bravantice**
    * Betonárna STETTER M2. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka konzistoměru (umístěn v rozvaděči).
    * Řídicí SW ATX B300.
    * Do provozu uvedeno v prosinci 2004.
    * Betonárna v roce 2010 přemístěna do Votic a je vedena pod novým záznamem.
* **82. ZAPA beton a.s., Brandýs nad Labem**
    * Rekonstrukce atypické betonárny. Dodávka řídicího a ovládacího elektrosystému. Dodávka konzistoměru (umístěn v rozvaděči).
    * Řídicí SW ATX B300
    * Do provozu uvedeno v září 2004.
    * V roce 2015 provedena rekonstrukce silové a ovládací části. Betonárna je vedena pod novým záznamem.
* **81. ZAPA beton a.s., Frýdek Místek**
    * Betonárna STETTER M1. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka konzistoměru (umístěn v rozvaděči).
    * Řídicí SW ATX B300
    * Do provozu uvedeno v srpnu 2004.
* **80. DOPRASTAV a.s., Senec**
    * Rekonstrukce betonárny STETTER H 0,5. Kompletní dodávka řídicího a ovládacího elektrosystému v provedení ATX COMFORT. Dodávka konzistoměru.
    * Řídicí SW ATX B300
    * Dispečerský SW ATX D300
    * Do provozu uvedeno v červenci 2004.
* **79. ANDRLA, Opava**
    * Rekonstrukce betonárny STASIS SB 20. Kompletní dodávka řídicího, ovládacího a silového elektrosystému v provedení ATX COMPACT.
    * Řídicí SW ATX B300
    * Do provozu uvedeno v červnu 2004.
    * Betonárna byla v srpnu 2011 přemístěna do Albrechtic, nový provozovatel Karel Säckl. Betonárna je vedena pod novým záznamem.
* **78. VOD-EKO a.s., Trenčín**
    * Rekonstrukce betonárny STASIS SB 20. Kompletní dodávka řídicího, ovládacího a silového elektrosystému v provedení ATX COMPACT.
    * Řídicí SW ATX B300
    * Dispečerský SW ATX D300
    * Do provozu uvedeno v červnu 2004.
    * Betonárna byla v průběhu let zrušena.
* **77. ZAPA beton a.s., Šenov**
    * Betonárna MERKO HBS 50. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka konzistoměru (umístěn v rozvaděči).
    * Řídicí SW ATX - B300
    * Do provozu uvedeno v květnu 2004.
* **76. ZAPA beton a.s., Praha - Kačerov III**
    * Betonárna MERKO. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka konzistoměru (umístěn v rozvaděči).
    * Řídicí SW ATX B300
    * Do provozu uvedeno v dubnu 2004.
    * V roce 2016 provedena rekonstrukce řízení. Betonárna je vedena pod novým záznamem.
* **75. ZAPA beton a.s., Cheb**
    * Rekonstrukce betonárny STETTER. Dodávka řídicího a ovládacího elektrosystému. Dodávka konzistoměru (umístěn v rozvaděči).
    * Řídicí SW ATX B300
    * Do provozu uvedeno v únoru 2004.
    * Stávajícím provozovatelem betonárny je Českomoravský beton a.s.
* **74. ZAPA beton a.s., Šumperk**
    * Betonárna MERKO HBS 50. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka konzistoměru (umístěn v rozvaděči).
    * Řídicí SW ATX B300
    * Do provozu uvedeno v únoru 2004.
* **73. ZAPA beton a.s., Říčany**
    * Betonárna STETTER M2. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka konzistoměru (umístěn v rozvaděči).
    * Řídicí SW ATX B300
    * Do provozu uvedeno v lednu 2004.

## 2003 ##

* **72. ZAPA beton a.s., České Budějovice**
    * Betonárna STETTER M2. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka konzistoměru (umístěn v rozvaděči).
    * Řídicí SW ATX 300
    * Do provozu uvedeno v listopadu 2003.
* **71. ZAPA beton a.s., Středokluky**
    * Rekonstrukce betonárny STASIS Ce-50. Dodávka řídicího a ovládacího elektrosystému. Dodávka konzistoměru (umístěn v rozvaděči).
    * Řídicí SW ATX 300
    * Do provozu uvedeno v říjnu 2003.
    * V únoru 2011 provedena kompletní inovace řídicího systému. Betonárna je vedena pod novým číslem.
* **70. COLORBETON a.s., Liberec**
    * Rekonstrukce věžové betonárny STETTER. Dodávka řídicího a ovládacího elektrosystému. Dodávka konzistoměru (umístěn v rozvaděči).
    * Řídicí SW ATX 300
    * Do provozu uvedeno v září 2003.
    * Betonárna je provozována firmou ZAPA Beton.
    * V roce 2012 proběhla rekonstrukce řídicího systému. Betonárna je vedena pod novým číslem.
* **69. ZAPA beton a.s., Hodonín**
    * Betonárna STETTER M1. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka konzistoměru (umístěn v rozvaděči).
    * Řídicí SW ATX 300
    * Do provozu uvedeno v září 2003.
* **68. TBG Louny s.r.o., Žatec**
    * Mobilní betonárna ELBA EMM 30. Betonárna přestěhována v roce 2003 i s řízením.
    * Řídicí SW ATX 300
    * Do provozu uvedeno v srpnu 2003.
    * Betonárna byla v průběhu let rekonsturována s jiným řízením.
* **67. ZAPA beton a.s., Benešov**
    * Betonárna ELBA EMC 60. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka konzistoměru (umístěn v rozvaděči).
    * Řídicí SW ATX 300
    * Do provozu uvedeno v srpnu 2003.
* **66. SKANSKA TRANSBETON s.r.o., Praha - Letňany**
    * Rekonstrukce betonárny STETTER. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka konzistoměru a vlhkoměrného zařízení ARNOLD - Automation.
    * Řídicí SW ATX 300
    * Do provozu uvedeno v červenci 2003.
* **65. ZAPA beton SK s.r.o., Piešťany**
    * Betonárna MERKO HBS 50. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka konzistoměru (umístěn v rozvaděči).
    * Řídicí SW ATX 300
    * Do provozu uvedeno v červnu 2003.
* **64. ZAPA beton SK s.r.o., Nové Mesto nad Váhom**
    * Betonárna MERKO HBS 50. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka konzistoměru (umístěn v rozvaděči).
    * Řídicí SW ATX 300
    * Do provozu uvedeno v květnu 2003.
* **63. DOPRABET v.d., Lánov**
    * Rekonstrukce atypické betonárny s míchačkou RTM 750 S 0,5 m3. Kompletní dodávka řídicího, ovládacího a silového elektrosystému v provedení ATX COMPACT.
    * Řídicí SW ATX 300
    * Do provozu uvedeno v dubnu 2003.
* **62. ZAPA beton a.s., Havlíčkův Brod**
    * Betonárna MERKO HBS 50. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka konzistoměru (umístěn v rozvaděči).
    * Řídicí SW ATX 300
    * Do provozu uvedeno v březnu 2003.
* **61. ZAPA beton a.s., Rychnov na Moravě**
    * Betonárna STETTER M2. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka konzistoměru (umístěn v rozvaděči) a vlhkoměrného zařízení ARNOLD - Automation.
    * Řídicí SW ATX 300
    * Do provozu uvedeno v únoru 2003.
    * Betonárna byla v roce 2006 přestěhována do Jihlavy a je vedena pod novým záznamem.
* **60. ZAPA beton a.s., Svitavy**
    * Betonárna MERKO HBS 50. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka konzistoměru (umístěn v rozvaděči).
    * Řídicí SW ATX 300
    * Do provozu uvedeno v lednu 2003.
* **59. ZAPA beton a.s., Neratovice**
    * Rekonstrukce řídicího systému betonárny ELBA EMC 60. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka konzistoměru (umístěn v rozvaděči).
    * Řídicí SW ATX 300
    * Do provozu uvedeno v lednu 2003.

## 2002 ##

* **58. SKANSKA TRANSBETON s.r.o., Praha - Holešovice**
    * Věžová betonárna firmy MERKO. Po úplném zničení betonárny při záplavě v srpnu 2002 kompletní nová dodávka řídicího, ovládacího a silového elektrosystému včetně dodávky dvojitého vlhkoměrného systému firmy ARNOLD - Automation.
    * Řídicí SW ATX 300
    * Do provozu uvedeno v říjnu 2002.
    * Betonárna byla v průběhu let odstavena z provozu.
* **57. SKANSKA TRANSBETON s.r.o., Brno - Horní Heršpice**
    * Betonárna MERKO. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka konzistoměru a vlhkoměrného zařízení ARNOLD - Automation.
    * Řídicí SW ATX 300
    * Do provozu uvedeno v září 2002.
* **56. SKANSKA TRANSBETON s.r.o., Kolín**
    * Betonárna STETTER M1,25. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka konzistoměru a vlhkoměrného zařízení ARNOLD - Automation.
    * Řídicí SW ATX 300
    * Do provozu uvedeno v červenci 2002.
* **55. ZAPA beton a.s., Kolín**
    * Mobilní betonárna STETTER M2. Kompletní dodávka řídicího, ovládacího a silového elektrosystému v provedení ATX MOBIL. Dodávka konzistoměru (umístěn v rozvaděči).
    * Řídicí SW ATX 300
    * Do provozu uvedeno v červnu 2002.
* **54. ZAPA beton a.s., Holubice**
    * Betonárna STASIS Ce-50. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka konzistoměru (umístěn v rozvaděči).
    * Řídicí SW ATX 300
    * Do provozu uvedeno v červnu 2002.
    * V únoru 2011 provedena kompletní inovace řídicího systému v souvislosti s rozšířením technologického zařízení betonárny. Betonárna je vedena pod novým záznamem.
* **53. B.G.M. holding Praha, Třebechovice**
    * Atypická betonárna. Kompletní dodávka řídicího, ovládacího a silového elektrosystému v provedení ATX MINI+.
    * Řídicí SW ATX 300
    * Do provozu uvedeno v dubnu 2002.
    * Současným provozovatelem je firma TANEX a.s.
* **52. ZAPA beton a.s., Vyškov**
    * Rekonstrukce řídicího systému atypické betonárny německé provenience. Kompletní dodávka řídicího a ovládacího elektrosystému a jeho propojení se stávajícím silovým rozvaděčem. Dodávka konzistoměru.
    * Řídicí SW ATX 300
    * Do provozu uvedeno v březnu 2002.
    * Betonárna prošla v roce 2015 rekonstrukcí řízení a je vedena pod novým záznamem.
* **51. BETON Šťavíček, Vranovice**
    * Mobilní betonárna ELBA EMM 30-III-vak 4. Kompletní dodávka řídicího, ovládacího a silového elektrosystému v provedení ATX COMPACT.
    * Řídicí SW ATX 300
    * Do provozu uvedeno v březnu 2002.
    * V roce 2003 betonárna přestěhovaná do Žatce, nový provozovatel TBG Louny. Betonárna vedena pod novým záznamem.
* **50. SKANSKA TRANSBETON s.r.o., Havlíčkův Brod**
    * Betonárna MERKO s míchačkou LIEBHERR 1 m3. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka konzistoměru a vlhkoměrného zařízení ARNOLD – Automation.
    * Řídicí SW ATX 300
    * Do provozu uvedeno v březnu 2002.
    * Betonárna byla v průběhu let odprodána a předělána na jiný řidicí systém.
* **49. STAVING spol. s r.o., Jičín**
    * Mobilní betonárna STROJBET 25. Kompletní dodávka řídicího, ovládacího a silového elektrosystému v provedení ATX COMPACT.
    * Řídicí SW ATX 300
    * Do provozu uvedeno v únoru 2002.
    * Dispečerský SW ATX D300. Od srpna 2007.
* **48. DYWIDAG Prefa Lysá nad Labem a.s., Lysá nad Labem**
    * Betonárna STASIS Ce-50. Kompletní dodávka řídicího, ovládacího, silového a informačního elektrosystému. Dodávka konzistoměru (umístěn v rozvaděči). Dodávka vlhkoměrného systému firmy ARNOLD - Automation.
    * Řídicí SW ATX 300
    * Do provozu uvedeno v únoru 2002.
    * V průběhu let byla betonárna zrušena.

## 2001 ##

* **47. ZAPA beton a.s., Brno - Přízřenice**
    * Mobilní betonárna STETTER M2. Kompletní dodávka řídicího, ovládacího a silového elektrosystému v provedení ATX MOBIL. Dodávka konzistoměru (umístěn v rozvaděči).
    * Řídicí SW ATX 300
    * Do provozu uvedeno v září 2001.
* **46. ILBAU spol. s r.o., Chrastava**
    * Betonárna ELBA EMC 30. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka konzistoměru.
    * Řídicí SW ATX 251
    * Do provozu uvedeno v květnu 2001.
    * V průběhu let betonárna rekonstruována s jiným řízením.
* **45. SMP CONSTRUCTION a.s., Brandýs nad Labem II**
    * Betonárna STETTER H1. Kompletní dodávka řídicího, ovládacího, informačního a silového elektrosystému. Dodávka vlhkoměrného systému firmy ARNOLD - Automation.
    * Řídicí SW ATX 300
    * Do provozu uvedeno v únoru 2001.
    * Právním nástupce SMP CONSTRUCTION a.s. je PREFA PRO, a.s.
    * V roce 2016 proběhla rekonstrukce vážicího systému betonárny a částečně elektroniky řízení.

## 2000 ##

* **44. MB - FRISCHBETON s.r.o., Mladá Boleslav**
    * Betonárna STASIS Ce-55. Kompletní dodávka řídicího, ovládacího a silového elektrosystému v provedení ATX COMPACT, rozšířené a doplněné zapojení pro registraci materiálu při ručním provozu, vlhkoměrné zařízení ARNOLD - Automation.
    * Řídicí SW ATX 251
    * Do provozu uvedeno v srpnu 2000.
    * V průběhu let betonárna rekonstruována s jiným řízením.
* **43. ZAPA beton a.s., Mladá Boleslav**
    * Mobilní betonárna STETTER M2. Kompletní dodávka řídicího, ovládacího a silového elektrosystému v provedení ATX MOBIL. Dodávka konzistoměru (umístěn v rozvaděči).
    * Řídicí SW ATX 300
    * Do provozu uvedeno v červenci 2000.
    * V roce 2019 byla betonárna nahrazena novým řízením. Betonárna je vedena pod novým záznamem.
* **42. ŽPSV Uherský Ostroh a.s., Čerčany**
    * Betonárna STASIS Ce-50. Kompletní dodávka řídicího, ovládacího a informačního elektrosystému a jeho propojení se stávajícím silovým rozvaděčem.
    * Řídicí SW ATX 300
    * Do provozu uvedeno v červnu 2000.
    * V roce 2007 byl stroj nahrazen dvoumíchačkovým jádrem. Betonárna je v referencích vedena pod novým záznamem.
* **41. ILBAU spol. s r.o., Ostrava**
    * Betonárna STETTER H1. Kompletní dodávka řídicího, informačního a silového elektrosystému.
    * Řídicí SW ATX 251
    * Do provozu uvedeno v dubnu 2000.
    * Betonárna je v současné době mimo provoz.
* **40. HOLCIM beton a.s., Jihlava**
    * Mobilní betonárna STETTER M2. Kompletní dodávka řídicího, ovládacího, informačního a silového elektrosystému v provedení ATX MOBIL. Dodávka konzistoměru (umístěn v rozvaděči) a vlhkoměrného zařízení ARNOLD - Automation.
    * Řídicí SW ATX 251
    * Do provozu uvedeno v březnu 2000.
    * V průběhu let betonárna rekonstruována s jiným řízením.
* **39. KÁMEN Zbraslav s.r.o., Kněževes**
    * Dvojitá betonárna SGME 320. Rekonstrukce řídicího systému a ovládacího elektrosystému. Dodávka konzistoměru (2 ks), vlhkoměrné zařízení ARNOLD - Automation, digitální zobrazovače vah.
    * Řídicí SW ATX 251
    * Do provozu uvedeno v únoru 2000.
    * V průběhu let betonárna rekonstruována s jiným řízením.

## 1999 ##

* **38. ZAPA beton a.s., Staré Město u Uherského Hradiště**
    * Betonárna HBS 50. Kompletní dodávka řídicího, ovládacího a silového elektrosystému. Dodávka konzistoměru (umístěn v rozvaděči).
    * Řídicí SW ATX 251
    * Do provozu uvedeno v říjnu 1999.
    * Betonárna byla v roce 2013 přemístěna do Strážnice a je v referencích vedena pod novým záznamem.
* **37. HOLCIM beton a.s., Praha - Ruzyně**
    * Mobilní betonárna STETTER M2. Kompletní dodávka řídicího, ovládacího, informačního a silového elektrosystému v provedení ATX MOBIL. Dodávka konzistoměru (umístěn v rozvaděči) a vlhkoměrného systému ARNOLD - Automation.
    * Řídicí SW ATX 251
    * Do provozu uvedeno v srpnu 1999.
    * Betonárna byla v průběhu let zrušena.
* **36. HOLCIM beton s.r.o., Bratislava**
    * Věžová betonárna STETTER. Kompletní dodávka řídicího, informačního ovládacího a silového elektrosystému. Dodávka vlhkoměrného systému firmy ARNOLD - Automation.
    * Řídicí SW ATX 251
    * Do provozu uvedeno v červnu 1999.
    * V průběhu let betonárna rekonstruována s jiným řízením.
* **35. AGROSTAV a.s., Lanškroun**
    * Betonárna STROJBET 25 stavebního závodu Lanškroun. Kompletní dodávka řídicího, informačního, ovládacího a silového elektrosystému.
    * Řídicí SW ATX 251
    * Do provozu uvedeno v dubnu 1999.
    * V srpnu 2007 proveden HW a SW upgrade betonárny na řídicí SW ATX 300 pro nového nájemce firmu ZAPA beton a.s.
    * Betonárna zrušena a nahrazena jinou v roce 2017.
* **34. READYMIX Bohemia s.r.o., Praha - Stodůlky**
    * Betonárna TEKA. Kompletní dodávka řídicího a ovládacího elektrosystému. Dodávka konzistoměru.
    * Řídicí SW ATX 251
    * Do provozu uvedeno v březnu 1999.
    * Roku 2012 proveden upgradě řídicího SW na ATX 300.
    * Roku 2018 provedena kompletní rekonstrukce ovládací a silové části řídicího systému. Betonárna je vedena pod novým záznamem.

## 1998 ##

* **33. ZAPA beton a.s., Olomouc**
    * Nově budovaná věžová betonárna MERKO VB 1,5, dodávka kompletního řídicího systému. Dodávka vlhkoměrného systému firmy ARNOLD - Automation.
    * Řídicí SW ATX 251
    * Do provozu uvedeno v létě 1998.
    * V květnu 2004 proveden HW a SW upgrade betonárny na řídicí SW ATX 300.
* **32. STAVBY SILNIC A ŽELEZNIC a. s., Praha - Řeporyje**
    * Betonárna STASIS SB-20 pro výrobu prefabrikátů. Kompletní dodávka, řídicího, ovládacího a silového systému. Dodávka vlhkoměrného systému firmy ARNOLD - Automation.
    * Řídicí SW ATX 251
    * Do provozu uvedeno na jaře 1998.
    * V průběhu let betonárna rekonstruována s jiným řízením.
* **31. SMP CONSTRUCTION a. s., Brandýs nad Labem I**
    * Betonárna STETTER K05. Kompletní dodávka řídicího, ovládacího a silového systému. Dodávka vlhkoměrného systému firmy ARNOLD - Automation.
    * Řídicí SW ATX 251
    * Do provozu uvedeno na jaře 1998.
    * Řídicí SW ATX 300
    * V únoru 2001 proveden HW a SW upgrade betonárny na řídicí SW ATX 300.
    * V roce 2016 provedena rekonstrukce vážicího systému a částečně elektroniky řízení.
    * Právním nástupcem SMP Construction a.s. je PREFA PRO, a.s.
* **30. READYMIX Bohemia s.r.o., Teplice**
    * Nová betonárna instalovaná firmou STAST Praha s.r.o. Kompletní dodávka řídicího, ovládacího a silového systému. Dodávka vlhkoměrného systému firmy ARNOLD - Automation.
    * Řídicí SW ATX 251
    * Do provozu uvedeno na jaře 1998.
    * Roku 2012 proveden upgradě řídicího SW na ATX 300.
    * Právním nástupcem firmy READYMIX Bohemia s.r.o. je CEMEX Czech Republic, s.r.o.
    * Roku 2018 provedena rekonstrukce ovládacího a vážicího systému.
* **29. ZAPA beton a.s., Přerov**
    * Reinstalovaná betonárna ELBA. Kompletní dodávka řídicího, ovládacího a silového systému.
    * Řídicí SW ATX 251
    * Do provozu uvedeno počátkem roku 1998.
    * V květnu 2004 proveden HW a SW upgrade betonárny na řídicí SW ATX 300.
    * Betonárna v roce 2009 odstavena z provozu.

## 1997 ##

* **28. ŽPSV Uherský Ostroh a.s., Litice nad Orlicí**
    * Dvě oddělená mísicí centra pro výrobu prefabrikátů. Kompletní dodávka dvou řídicích, ovládacích a silových systémů včetně dodávky dvou vlhkoměrných systémů firmy ARNOLD - Automation.
    * Řídicí SW ATX 250
    * Do provozu uvedeno na podzim 1997.
    * Roku 2007 kompletní rekonstrukce ovládací a částečná silové části. Betonárna je v referencích vedena pod novým záznamem.
* **27. B & BC a. s., závod Prefa Zbůch III**
    * Atypické nově vybudované dvoumíchačkové mísicí jádro řízené dvojitým řídicím systémem.
    * Řídicí SW ATX 250
    * Do provozu uvedeno na podzim 1997.
    * Betonárna byla v průběhu let zrušena.
* **26. SKANSKA TRANSBETON s.r.o., Praha - Holešovice**
    * Nová věžová betonárna firmy MERKO. Kompletní dodávka řídicího, ovládacího a silového systému včetně dodávky dvojitého vlhkoměrného systému firmy ARNOLD - Automation.
    * Řídicí SW ATX 251
    * Do provozu uvedeno na podzim 1997.
    * V květnu 2000 proveden HW a SW upgrade betonárny na řídicí SW ATX 300.
    * Roku 2002 po záplavách kompletní rekonstrukce ovládací a částečná silové části. Betonárna je v referencích vedena pod novým záznamem.
* **25. VOJENSKÉ STAVBY a. s., firma POZISTAV - Hradec Králové**
    * Betonárna atypické konstrukce. Kompletní dodávka informačního, řídicího a silového systému.
    * Řídicí SW ATX 250
    * Do provozu uvedeno v létě 1997.
    * Betonárna odstavena z provozu.
* **24. STAVBY SILNIC A ŽELEZNIC a. s., Řevnice**
    * Betonárna švýcarské provenience firmy AMMANN, užívaná k výrobě betonové směsi pro stavební dílce. Kompletní dodávka informačního, řídicího a silového systému.
    * Řídicí SW ATX 250
    * Do provozu uvedeno v létě 1997.
    * V prosinci 2001 proveden HW a SW upgrade betonárny na řídicí SW ATX 251.
    * Dále bylo provedeno rozšíření řídicího systému o dávkování barev, měření vlhkosti Liaporu a úpravy související s dopravou betonu do haly.
    * V průběhu let betonárna rekonstruována s jiným řízením.
* **23. READYMIX Bohemia s.r.o., Hradec Králové - Plačice**
    * Reinstalovaná betonárna typu RANGER francouzské provenience. Kompletní dodávka řídicího, ovládacího a silového systému.
    * Řídicí SW ATX 250
    * Do provozu uvedeno na jaře 1997.
    * Roku 2010 kompletní rekonstrukce ovládací a částečná silové části při přestavbě betonárny. Betonárna je v referencích vedena pod novým záznamem.
* **22. MANE Holding a.s., České Budějovice**
    * Reinstalovaná betonárna STASIS Ce-50. Kompletní dodávka řídicího, ovládacího a silového systému.
    * Řídicí SW ATX 250
    * Do provozu uvedeno na jaře 1997.
    * Roku 2005 rekonstrukce s jiným řízením.
* **21. ZAPA beton a.s., Hulín**
    * Betonárna STASIS Ce-55. Dodávka kompletního řídicího systému, rekonstrukce ovládací a silové části.
    * Řídicí SW ATX 250
    * Do provozu uvedeno počátkem roku 1997.
    * V květnu 2004 proveden HW a SW upgrade betonárny na řídicí SW ATX 300.
    * Roku 2006 betonárna zrušena.

## 1996 ##

* **20. READYMIX Bohemia s.r.o.**
    * Reinstalovaná betonárna STASIS Ce-50. Kompletní dodávka řídicího, ovládacího a silového systému.
    * Dodávka systému uživateli provedena na podzim 1996.
    * Původně určeno pro lokalitu Písnice - betonárna nebyla nikdy uvedena do provozu.
* **19. SKANSKA TRANSBETON s.r.o., Praha - Ruzyně**
    * Atypická nová betonárna se zásobníkem kameniva STETTER, skipem s měkkým rozběhem (měnič) a míchačkou STASIS 1 m3. Kompletní dodávka řídicího, ovládacího, silového a dispečerského systému včetně dodávky dvojitého vlhkoměrného systému firmy ARNOLD - Automation.
    * Řídicí SW ATX 250
    * Do provozu uvedeno na jaře 1996.
    * V květnu 2000 proveden HW a SW upgrade betonárny na řídicí SW ATX 300.
    * Roku 2014 kompletní rekonstrukce ovládací a částečná silové části. Betonárna je v referencích vedena pod novým záznamem.
* **18. ZAPA beton a.s., Praha - Kačerov II**
    * Nová betonárna STETTER s míchačkou BAS 2 m3. Kompletní dodávka řídicího, ovládacího a silového systému.
    * Řídicí SW ATX 250
    * Do provozu uvedeno na jaře 1996.
    * Řídicí SW ATX 300
    * V květnu 2004 proveden HW a SW upgrade betonárny na řídicí SW ATX 300.
    * Roku 2012 kompletní rekonstrukce ovládací a silové části. Betonárna je v referencích vedena pod novým záznamem.
* **17. READYMIX Bohemia s.r.o., Kolín**
    * Skipová atypická betonárna s míchačkou STASIS 1 m3, původně s manuálním řízením. Rekonstrukce váhy vody, kompletní rekonstrukce ovládání.
    * Řídicí SW ATX 250
    * Do provozu uvedeno na jaře 1996.
    * V roce 2003 zprovozněn jiný řídicí systém.
* **16. B & BC a. s., závod Prefa - Zbůch II**
    * Betonárna STASIS Ce-55 používaná pro výrobu betonové směsi pro prefabrikáty. Kompletní rekonstrukce ovládání.
    * Řídicí SW ATX 250
    * Do provozu uvedeno v březnu 1996. V březnu 1998 provedena na přání zákazníka doinstalace dispečerského modulu.
    * V roce 2013 proveden upgrade na řídicí a dispečerský SW ATX 300.
* **15. READYMIX Bohemia s.r.o., Liberec**
    * Betonárna STASIS Ce-50 s původním poloautomatem atypické konstrukce. Kompletní rekonstrukce ovládání.
    * Řídicí SW ATX 250
    * Do provozu uvedeno na jaře 1996.
    * Roku 2005 rekonstrukce ovládací části. Silovou část dodala jiná firma. Betonárna je v referencích vedena pod novým záznamem.
* **14. READYMIX Bohemia s.r.o., Hradec Králové - Správčice**
    * Betonárna STASIS Ce-50 s původně manuálním ovládáním. Kompletní rekonstrukce ovládání.
    * Řídicí SW ATX 250
    * Do provozu uvedeno počátkem roku 1996.
    * Roku 2012 upgrade řídicího SW na ATX 300
    * Právním nástupcem firmy READYMIX Bohemia s.r.o. je CEMEX Czech Republic, s.r.o.
    * Roku 2017 kompletní rekonstrukce ovládací a silové části. Betonárna je v referencích vedena pod novým záznamem.

## 1995 ##

* **13. HOLCIM beton a.s., Doksany**
    * Reinstalovaná betonárna STASIS Ce-50, původně s jiným automatickým řízením. Kompletní rekonstrukce ovládání.
    * Řídicí SW ATX 210
    * Do provozu uvedeno na podzim 1995.
    * Betonárna v roce 2001 přemístěna do lokality Benešov u Prahy.
    * V průběhu let betonárna rekonstruována s jiným řízením.
* **12. READYMIX Bohemia s.r.o., Praha - Hloubětín**
    * Betonárna STASIS Ce-50 původně s manuálním řízením. Kompletní rekonstrukce ovládání. Nově instalována váha vody.
    * Řídicí SW ATX 250
    * Do provozu uvedeno na jaře roku 1995. V roce 1996 proveden upgrade na současný model, umožňující začlenění do sítě firmy READYMIX. Začátkem roku 1997 provedeno přestěhování řídicího pultu do zvláštního velínu mimo stroj.
    * Betonárna jako výrobní jednotka momentálně není v provozu.
* **11. SKANSKA TRANSBETON s.r.o., Praha - Řeporyje**
    * Betonárna STASIS Ce-50 původně s manuálním řízením. Kompletní rekonstrukce ovládání. Řídicí systém nové generace integruje výrobní a informační jednotku do jednoho celku.
    * Řídicí SW ATX 250
    * Do provozu uvedeno počátkem roku 1995.
    * Betonárna je mimo provoz.
* **10. READYMIX Bohemia s.r.o., Praha - Horní Počernice**
    * Betonárna STASIS Ce-50 původně s manuálním řízením. Kompletní rekonstrukce ovládání. Nově instalována váha vody.
    * Řídicí SW ATX 250
    * Do provozu uvedeno počátkem roku 1995. V roce 1996 proveden upgrade na současný model, umožňující začlenění do sítě firmy READYMIX.
    * Roku 2006 betonárna odstavena z provozu.
* **9. READYMIX Bohemia s.r.o., Pardubice**
    * betonárna přestěhována z Nového Boru
    * Betonárna STASIS SB-20 původně s manuálním řízením. Kompletní rekonstrukce ovládání. Nově instalované dávkování vody.
    * Řídicí SW ATX 250
    * Do provozu uvedeno počátkem roku 1995. V roce 1996 proveden upgrade na současný model, umožňující začlenění betonárny do sítě firmy READYMIX.
    * Betonárna zrušena a nahrazena novou roku 2008.
* **8. HIROSTAVBET s.r.o., Trnava**
    * Betonárna STASIS Ce-50 původně s manuálním řízením. Kompletní rekonstrukce ovládání, nově instalované dávkování tekutých přísad. Řídicí systém spolupracuje se vzdáleným dispečinkem.
    * Řídicí SW ATX 250
    * Do provozu uvedeno počátkem roku 1995. Na jaře roku 1997 proveden upgrade systému na současný model spolu s rekonstrukcí velínu po vyhoření v důsledku sabotáže konkurence.
    * Od roku 2013 betonárna odstavena.

## 1994 ##

* **7. B & BC a.s., závod Prefa - Zbůch I (transportbeton)**
    * Reinstalovaná betonárna STASIS Ce-50. Rekonstrukce silové části a kompletní rekonstrukce ovládání. Nově instalované dávkování tekutých přísad. Řídicí systém spolupracuje se vzdáleným dispečinkem.
    * Řídicí SW ATX 250
    * Do provozu uvedeno v létě 1994. V průběhu roku 1997 proveden upgrade systému na současný model z důvodu sjednocení ovládání s dalšími nově instalovanými systémy.
    * V roce 2013 proveden upgrade na řídicí a dispečerský SW ATX 300.
    * Betonárna zrušena roku 2012
* **6. ZAPA beton a.s., Praha - Kačerov I**
    * Betonárna STASIS Ce-50 původně s manuálním řízením. Kompletní rekonstrukce ovládání. Řídicí systém spolupracuje se vzdáleným dispečinkem.
    * Řídicí SW ATX 250
    * Do provozu uvedeno na jaře 1994. Počátkem roku 1995 kompletně rekonstruován systém vážení, instalace nového odprášení míchačky. V roce 1996 proveden upgrade systému na současný model pro umožnění povelování dvou betonáren z jednoho vzdáleného dispečinku.
    * V roce 2004 betonárna STASIS zrušena a nahrazena novou betonárnou MERKO s řídicím SW ATX 300.

## 1993 ##

* **5. READYMIX Bohemia s.r.o., Děčín**
    * Reinstalovaná betonárna STASIS Ce-50. Kompletně rekonstruované ovládání.
    * Řídicí SW ATX 250
    * Do provozu uvedeno na podzim 1993. V roce 1996 proveden upgrade systému na současný model, umožňující začlenění betonárny do sítě firmy READYMIX.
    * Roku 2012 upgrade řídicího SW na ATX 300
    * Právním nástupcem firmy READYMIX Bohemia s.r.o. je CEMEX Czech Republic, s.r.o.
    * Roku 2017 kompletní rekonstrukce ovládací a silové části. Betonárna je v referencích vedena pod novým záznamem.
* **4. TBG METROSTAV s.r.o., Praha - Rohanský ostrov**
    * Betonárna STETTER původně s děrnoštítkovým řízením. Rekonstrukce ovládání, nově instalované dávkování tekutých přísad. Přímé propojení výroby se vzdáleným dispečinkem a fakturačním systémem. Sběr dat společně s betonárnou Zličín.
    * Řídicí SW ATX 250
    * Do provozu uvedeno na jaře 1993.
    * Začátkem roku 1999 betonárna rekonstruována jiným řízením, umožňujícím začlenění do sítě TBG.
* **3. READYMIX Praha - Malešice s.r.o.**
    * Mísicí jádro pro panelovou výrobu a expedici transportbetonu se dvěma míchačkami a vozíkovou dopravou směsi na místo zpracování. Obsluha výrobních linek dálkově zadává požadavky na betonovou směs do řídicího systému. Kompletní rekonstrukce ovládací části.
    * Řídicí SW ATX 250
    * Do provozu uvedeno počátkem roku 1993. Na jaře 1995 instalován nový systém zásobování cementem, rekonstrukce vah kameniva a cementu. V průběhu roku 1996 instalována nová váha vody čisté a kalové, nové dávkovače přísad.
    * Začátkem roku 1997 provedena další celková rekonstrukce silové a ovládací elektročásti včetně kabeláže a proveden hardwarový a softwarový upgrade spolu s přemístěním velínu do budovy mimo vlastní stroj.
    * Roku 2002 betonárna rekonstruována s jiným řízením.
* **2. LACERTA a. s., závod Prefa, Brandýs nad Labem - velké mísicí jádro**
    * Mísicí jádro pro panelovou výrobu se třemi míchačkami a vozíkovou dopravou směsi na místo zpracování. Obsluha výrobních linek dálkově zadává požadavky na betonovou směs do řídicího systému. Kompletní rekonstrukce ovládací části, nově vybavený velín.
    * Do provozu uvedeno počátkem roku 1993. Na jaře 1994 rekonstruován systém navažování kameniva.
    * Betonárna je v současné době mimo provoz.

## 1992 ##

* **1. LACERTA a. s., závod Prefa, Brandýs nad Labem - malé mísicí jádro**
    * Betonárna STASIS SB-20, nově řešené dávkování vody. Kompletní rekonstrukce ovládací části, nově konstruovaný pult s řídicím systémem.
    * Do provozu uvedeno v říjnu 1992.
    * Betonárna je v současné době mimo provoz.
