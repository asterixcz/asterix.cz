Title: Kontakt
order: 05

# Kontakt #

**provozovna a servisní středisko Praha:**  
Asterix a.s.  
V Hájích 517/28  
149 00 Praha 4 - Háje  
[(odkaz na mapu)](http://www.mapy.cz/s/hQ3v)

Tel.: +420 272 917 086

E-Mail: asterix@asterix.cz  
Web: http://asterix.cz

IČ: 43872298  
DIČ: CZ43872298  

**servisní technik pro Moravu a Slovensko:**  
Pavlovova 50  
700 30 Ostrava 3  
[(odkaz na mapu)](https://mapy.cz/s/lokesupato)

**servisní technici**  
Tel.: +420 910 125 024

**sídlo společnosti:**  
Nad kolonií 693/29  
140 00 Praha 4 - Podolí  

Obchodní rejstřík:  
vedený Městským soudem v Praze
oddíl B, vložka 1021
