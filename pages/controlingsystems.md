Title: Řídicí systémy betonáren
attribute: 2
order: 02

# Řídicí systémy betonáren #

## ATX 400 s kompaktním rozvaděčem ##

Inovativní řídicí systém kompatibilní s nejnovějšími verzemi Windows. Ovládací program byl navržený tak, aby obsluha dostávala pouze ty informace, které potřebuje. Zároveň obsluhu zvládne i osoba s minimální zkušeností s ovládáním počítače.

Systém je kompatibilní se všemi druhy betonáren libovolného výrobce a splňuje požadavky normy EN-206+A2. Jedná se o vyspělý systém, který má již v základu vše, co standardní betonárna potřebuje. Hlavní důraz je kladen na přesnost navažování, využití potenciální rychlosti betonárny, spolehlivost systému, diagnostické metody pro určování závad a kvalitní a dostupný servis.

Dodávka obsahuje kompaktní rozvaděč se silovými, ovládacími prvky, vstupně výstupní jednotkou a vážicími zobrazovači. Elektronika rozvaděče je spojena pomocí ethernetového rozhraní s řídicím počítačem. Promyšlená koncepce dává možnost rozšíření betonárny o další sila materiálů a jiné prvky za minimální náklady.

Při projektování řídicích systémů je kladen důraz, aby byly udržovatelné a servisovatelné po dobu 20-ti let. I proto je řídicí program je umístěn přímo v počítači, což umožňuje snadnou konfiguraci, rozšířitelnost, diagnostiku závad a servis. Klademe velký důraz na dokumentaci, která musí být přehledná a aktuální. S ohledem na dlouhou životnost a spolehlivost řídicího systému tak dosahuje zákazník maximálního užitku.


Výhody:

* celoživotní záruka na I/O (vstupně-výstupní) jednotku
* přesné navažování díky adaptivním algoritmům
* vysoká přesnost navažování i malých požadavků na vážení
* využití maximálního výkonu betonárny bez prostojů
* minimální poruchovost
* snadnou rozšiřitelnost o další technologické prvky (sila, sondy, vibrátory ...)
* možnost snadných upgradů software po neomezenou dobu díky zachovávání zpětné kompatibility se staršími produkty
* vzdálené připojení pro diagnostiku a servis
* diagnostické nástroje umožňující určit závady i mechanických částí betonárny
* přehledná dokumentace
* směšování teplé a studené vody, respektování hustoty kalové vody, zobrazení vodního součinitele dle aktuálně navážených materiálů
* vhodné pro výrobu betonu z recyklovaného kameniva (splňuje požadavky pro výrobu Rebetongu)
* možnost výroby betonu z libovolného počtu cementů
* sledování provozních hodin míchačky a zdvihů skipu
* pro přesné určení konzistence směsi je použit třífázový konzistoměr
* ovládací počítač je ve fanless provedení bez mechanických částí
* jsou použity pouze kvalitní komponenty renomovaných výrobců (Intel, Schneider Electric, Siemens, Schrack...)
* použití nejmodernější technologie v podobě polovodičových relé
* kontrola kvality dodávaných produktů (certifikát ISO 9001)
* dlouhodobá životnost řídicího systému jako celku (nejméně 15 let, v praxi ale běžně přes 20 let)
* v rámci dvouleté záruky servis řídicího systému s vyřešením problému do 24 hodin, poté je možné si stejnou službu přikupovat za měsíční paušální poplatek
* rychlý a účinný servis díky dvou servisním střediskům (v Praze a Ostravě)


[![ATXB300](/images/thumbs/atxb300.jpg)](/images/atxb300.jpg)

## Možné alternativy a doplnění ##

**ATX Comfort** - určený pro rekonstrukci betonárny se zachováním silové části.

[![ATX Comfort](/images/thumbs/comfort.jpg)](/images/comfort.jpg)

**ATX D300** - dispečerský program pro zásobování řídicího programu objednávkami, obsahuje různé databáze (receptury, dodavatelé, odběratelé, zakázky, stavby, čerpadla...). Dispečerský program vytváří různé statistiky o výrobě a spotřebě materiálu, tiskne dodací listy a jeho výstup je připravený, aby se přes softwarový převodník spojil s libovolným účetním softwarem.

[![ATXD300](/images/thumbs/atxd300.jpg)](/images/atxd300.jpg)
